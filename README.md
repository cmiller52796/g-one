# PSU-GOne

Built in Unity version 2021.3.8f1

G-One is a short, simple 3D racing game inspired in purpose by F-Zero GX by Nintendo (2003), the last 3D entry in a franchise that Nintendo seems to have forgotten about with the exception of putting Captain Falcon in Super Smash Brothers.

* Basics...
    * Three tracks with unique geometry ([satisfies User Stories 5 and 9](#satisfied-user-stories))
    * Simple mechanics to be playable by keyboard.
    * High speed, requiring twitchy driving and watching your speed.
<img src="DocImages/Preview.png" width="600"/>

## Modes

* Time Attack ([satisfies User Story 4](#satisfied-user-stories))
    * Race around the track of your choice as many times as you want (completing a race is considered making 3 laps around the track, laps are displayed on the top right of your screen).
    * Your best times for each track are stored as persistent data so that whenever you quit the game, you can open it back up again and try to beat your personal record (displayed on top left of screen)!
* Versus
    * DESCOPED due to other deadlines outside this project

## Controls

([collectively satisfy User Story 5](#satisfied-user-stories))

* Accelerate - Spacebar 
    * Hold down your spacebar and accelerate to your standard speed of 1300m/s!
    * Your current speed is displayed on the lower right of your screen. ([satisfies User Story 7](#satisfied-user-stories))
* Boost - hold "q" ([satisfies User Story 3](#satisfied-user-stories))
    * Hold down this key while accelerating to hit your maximum speed of 2600m/s!
    * Whilst holding this down, you'll lose health.
        * You cannot boost your way into 0 health, but you can get awfully close!
    * Acceleration while boosting is increased dramatically, and your deceleration is also decreased (if you boost but dont hit spacebar, you won't slow down as fast).
* Turn - Left/Right arrow keys
    * The steering is linear!  So tapping your arrow keys to make finite adjustments is something you'll do often!
* Drift turn - hold "e" and execute a turn
    * This is simply a multiplier applied to your turning, good necessary for making sharp turns at high speeds!

## Mechanics

* Health (top right of screen) ([satisfies User Story 3](#satisfied-user-stories))
    * Drained via boosting and hitting walls, if you run out of health, you'll be prompted with a retry screen!
    * Replinished via the light teal pads, every track has one and going over it/through it will regenerate your health.
* Checkpoints/Lap Counting (top right of screen)
    * The physics may be janky, but rest assured you cannot skip entire sections of the track and get absurdly fast times.
    * There are invisible checkpoints around the track, if you fail to hit them all on a lap, your lap counter wont increment until you go back through them!
    * Once completing a race, you'll get prompted with a retry screen!
* Zero-G Driving
    * Ever want to race around a cylinder?  Now you can!  This is the future, requiring Earth's gravity to stay on the road is a thing of the past!
* Persistent Data for Personal Bests ([satisfies User Story 4](#satisfied-user-stories))
    * To facilitate Time Attack, your best time per track is saved locally on your machine
        * Path: `C:\Users\\{your_user_name}\AppData\LocalLow\CarterMiller\GOne`
            * If this is deleted, your best times are reset!
* Death Barriers
    * If you ever fly off the track too far, you'll get prompted with a retry screen!

## General Notes

* High-ish skill ceiling! ([satisfies User Story 6](#satisfied-user-stories))
    * It is possible on every track to hold the accelerator the whole time and complete the laps, boosting whenever you get a chance, but you gotta act fast!
* 3rd person camera
    * Driving in 1st person would have been extremely jarring, so like F-Zero GX a 3rd person persective is used for maximum visibility of where you are going and how fast you're going there! ([satisfies User Story 8](#satisfied-user-stories))

## How to Play

1. Go to the [releases section](https://github.com/CarterSimMiller/PSU-GOne/releases), and download the latest version (only one at the time of writing) zip file (should be around 50ish MB in size).
<img src="DocImages/ReleasesTab.png" width="800"/>

2. Unzip the folder wherever you wish, double-click GOne.exe, and you'll be prompted with the Main Menu screen (and some rad music). ([satisfies User Story 1 and 2](#satisfied-user-stories))  
2a. Windows may warn you that this is a virus, just click "More info" and "Run anyway", I'm not sure how I'd prevent this without some code signing, and I'm not sure how to do that...  
<img src="DocImages/Menu.png" width="600"/>

3. Select Time Attack and you'll be prompted with the Track Select screen, pick your track and the countdown should begin (as well as even more sick music)! ([satisfies User Story 1 and 2](#satisfied-user-stories))
<img src="DocImages/TrackSelect.png" width="600"/>
<img src="DocImages/StartRace.png" width="600"/>

4. Once the countdown is complete, race to your heart's content, retrying as much as you wish or cycling back to the main menu after you crash one too many times!  
4a. See the Controls and Mechanics section for a better understanding!

## Satisfied User Stories

1. [As a user, I want a menu system to easily transition between modes and tracks.](https://github.com/CarterSimMiller/PSU-GOne/milestone/9)
2. [As a user, I want a pleasant audio/visual experience whilst playing.](https://github.com/CarterSimMiller/PSU-GOne/milestone/10)
3. [As a user, I want a mechanic that introduces a risk/reward, to increase my level of suspense while playing.](https://github.com/CarterSimMiller/PSU-GOne/milestone/6)
4. [As a user, I want to race free of other vehicles, and just try to beat my own best time on a track.](https://github.com/CarterSimMiller/PSU-GOne/milestone/1)
5. [As a user, I want to race a virtual vehicle in a 3D environment at high speeds so that I can experience a "rush".](https://github.com/CarterSimMiller/PSU-GOne/milestone/5)
6. [As a user, I want to be able to race around a track without hitting walls so I can maintain my speed.](https://github.com/CarterSimMiller/PSU-GOne/milestone/3)
7. [As a user, I want to see how fast I'm going to keep me engaged.](https://github.com/CarterSimMiller/PSU-GOne/milestone/8)
8. [As a user, I want the camera to follow my vehicle in 3rd person so I can always see where my vehicle is relative to the course.](https://github.com/CarterSimMiller/PSU-GOne/milestone/4)
9. [As a user, I want to race on tracks that feature "zero-g" maneuvers, such as driving upside down, so that I can heighten my experience.](https://github.com/CarterSimMiller/PSU-GOne/milestone/7)
