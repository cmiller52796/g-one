using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class CustomSceneManager : MonoBehaviour {
    [System.NonSerialized] public Manager.GameMode mode;
    [System.NonSerialized] public string currentScene;
    [System.NonSerialized] public string trackScene;
    [System.NonSerialized] public Manager mgr;
    [System.NonSerialized] public static GameObject singleton;

    private void Awake() {
        DontDestroyOnLoad(this.gameObject);
        if (singleton != null && singleton != this) {
            Destroy(this.gameObject);
            return;
        }
        else if (GameObject.FindObjectOfType<AudioManager>() == null){
            singleton = this.gameObject;
        }
        else {
            singleton = GameObject.Find("CustomSceneManager");
        }
        SceneManager.sceneLoaded += Init;
        VehicleMgr.objectiveDone += LoadRetryComplete;
        Vehicle.NoHealth += LoadRetryCrash;
    }

    void OnEnable() {
        SceneManager.sceneLoaded += Init;
        VehicleMgr.objectiveDone += LoadRetryComplete;
        Vehicle.NoHealth += LoadRetryCrash;
    }

    void OnDisable() {
        SceneManager.sceneLoaded -= Init;
        VehicleMgr.objectiveDone -= LoadRetryComplete;
        Vehicle.NoHealth -= LoadRetryCrash;
    }

    public void Init(Scene scene, LoadSceneMode scene_mode) {
        currentScene = scene.name;
        switch(scene.name) {
            case "MainMenu":
                break;
            case "Track1":
                mgr = GameObject.FindObjectOfType<Manager>();
                mgr.mode = mode;
                trackScene = scene.name;
                break;
            case "Track2":
                mgr = GameObject.FindObjectOfType<Manager>();
                mgr.mode = mode;
                trackScene = scene.name;
                break;
            case "Track3":
                mgr = GameObject.FindObjectOfType<Manager>();
                mgr.mode = mode;
                trackScene = scene.name;
                break;
        }
    }

    void LoadRetryComplete() {
        SceneManager.LoadScene("RetryComplete");
    }

    void LoadRetryCrash() {
        SceneManager.LoadScene("RetryCrash");
    }
}
