using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour {
    [System.NonSerialized] public int setFrameRate = 60;
    [System.NonSerialized] public bool objectiveComplete = false;

    public enum GameMode {
        Default,
        TimeAttack,
        Versus,
        None,
    }

    [System.NonSerialized] public GameMode mode = GameMode.None;

    public GameObject uiObj;
    public GameObject timerObj;
    [System.NonSerialized] public Timer timerComp;
    [System.NonSerialized] public PlayerUI playerUIComp;
    public delegate void UIInitCb(Manager.GameMode mode);
    public UIInitCb callback;

    public void Awake() {
        // Limit the frame rate to 60FPS
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = setFrameRate;
    }

    public void Start() {
        timerComp = timerObj.GetComponent<Timer>();
        playerUIComp = uiObj.GetComponent<PlayerUI>();
        if (callback == null) { callback = playerUIComp.Init; }
        callback(mode);
    }

    void OnEnable() {
        VehicleMgr.objectiveDone += ObjectiveDone;
    }

    void OnDisable() {
        VehicleMgr.objectiveDone -= ObjectiveDone;
    }

    void ObjectiveDone() {
        if (mode == GameMode.TimeAttack) {
            if(Utils.UpdateBestTime(SceneManager.GetActiveScene().name, timerComp.currentTime)) {
                print("New best time!");
            }
        }
        else if (mode == GameMode.Versus) {
            print("Versus mode not implemented!");
        }
        objectiveComplete = true;
    }
}
