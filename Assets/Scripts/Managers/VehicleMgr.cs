using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class VehicleMgr : MonoBehaviour {
    [System.NonSerialized] public int currentCheckPointIdx = 0;
    [System.NonSerialized] public int currentLapNum = 1;
    [System.NonSerialized] public int maxLaps = 3;

    public static event Action objectiveDone;

    void OnEnable() {
        Checkpoint.checkObjective += CheckObjective;
    }

    void OnDisable() {
        Checkpoint.checkObjective -= CheckObjective;
    }

    void CheckObjective() {
        if (currentLapNum > maxLaps) {
            FireObjectiveDone();
        }
    }

    public void FireObjectiveDone() {
        objectiveDone?.Invoke();
    }
}