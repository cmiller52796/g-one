using UnityEngine;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;

public static class DataMgr {
    public static string saveDir = "/persistData/";
    public static string saveFile = "data.txt";
    public static string fullSaveDir = Application.persistentDataPath + saveDir;
    public static string fullSavePath = fullSaveDir + saveFile;

    public static void SaveData(PersistData data) {
        if (!Directory.Exists(fullSaveDir)) {
            Directory.CreateDirectory(fullSaveDir);
        }
        string jsonDump = JsonConvert.SerializeObject(data.trackTimeDict);
        File.WriteAllText(fullSavePath, jsonDump);
    }

    public static PersistData LoadData() {
        if (File.Exists(fullSavePath)) {
            string jsonDump = File.ReadAllText(fullSavePath);
            return new PersistData(JsonConvert.DeserializeObject<Dictionary<string, TrackTime>>(jsonDump));
        }
        Debug.Log("No pre-existing save file.");
        return new PersistData();
    }
}