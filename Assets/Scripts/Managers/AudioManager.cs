using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour {
    public AudioSource audioSource;
    public AudioClip menuMusic;
    public AudioClip trackOneMusic;
    public AudioClip trackTwoMusic;
    public AudioClip trackThreeMusic;

    [System.NonSerialized] public static GameObject singleton;
    [System.NonSerialized] public Dictionary<string, AudioClip> trackMusicDict;

    private void Awake() {
        DontDestroyOnLoad(this.gameObject);
        if (singleton != null && singleton != this) {
            Destroy(this.gameObject);
            return;
        }
        else if (GameObject.FindObjectOfType<AudioManager>() == null){
            singleton = this.gameObject;
        }
        else {
            singleton = GameObject.Find("AudioManager");
        }
        trackMusicDict = new Dictionary<string, AudioClip>();
        trackMusicDict["MainMenu"] = menuMusic;
        trackMusicDict["Track1"] = trackOneMusic;
        trackMusicDict["Track2"] = trackTwoMusic;
        trackMusicDict["Track3"] = trackThreeMusic;
    }

    void OnEnable() {
        SceneManager.sceneLoaded += Init;
    }

    void OnDisable() {
        SceneManager.sceneLoaded -= Init;
    }

    public void Init(Scene scene, LoadSceneMode scene_mode) {
        switch(scene.name) {
            case "MainMenu":
            case "Track1":
            case "Track2":
            case "Track3":
                StopThenPlay(trackMusicDict[scene.name]);
                break;
        }
    }

    void StopThenPlay(AudioClip clip) {
        audioSource.Stop();
        audioSource.clip = clip;
        audioSource.Play();
    }
}