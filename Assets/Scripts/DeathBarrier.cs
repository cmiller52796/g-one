using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathBarrier : MonoBehaviour {
    private void OnTriggerEnter(Collider collision) {
        Vehicle vehicle = collision.gameObject.GetComponent<Vehicle>();
        if (vehicle) {
            if (collision.gameObject.tag == "Player") {
                SceneManager.LoadScene("RetryCrash");
            }
        }
    }
}