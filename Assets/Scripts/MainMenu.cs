using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public CustomSceneManager sceneMgr;
    [System.NonSerialized] public int setFrameRate = 60;
    [System.NonSerialized] public bool quitTriggerred = false;

    public void Awake() {
        // Limit the frame rate to 60FPS on menu
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = setFrameRate;
    }

    public void TimeAttack() {
        sceneMgr.mode = Manager.GameMode.TimeAttack;
        SceneManager.LoadScene("TrackSelect");
    }

    public void QuitGame() {
        // I shouldn't have to use this boolean for testing, as there is an
        // event that gets fired when quit gets called, but it doesn't seem
        // to be triggering how I expect, so this is fine 
        quitTriggerred = true;
        Application.Quit();
    }
}
