using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueFalconStats : VehStatsIntfc {
    private float _normalMaxSpeed = 1300f;
    public float normalMaxSpeed { get=>_normalMaxSpeed; set=>_normalMaxSpeed = value;}

    private float _boostAddSpeed = 1300f;
    public float boostAddSpeed { get=>this._boostAddSpeed; set=>this._boostAddSpeed = value;}

    private float _normalAccel = 400f;
    public float normalAccel { get=>this._normalAccel; set=>this._normalAccel = value;}
    private float _normalDecel = 550f;
    public float normalDecel { get=>this._normalDecel; set=>this._normalDecel = value;}
    private float _boostDecel = 200f;
    public float boostDecel { get=>this._boostDecel; set=>this._boostDecel = value;}
    private float _boostAddAccel = 800f;
    public float boostAddAccel { get=>this._boostAddAccel; set=>this._boostAddAccel = value;}

    private float _desiredHeight = 14f;
    public float desiredHeight { get=>this._desiredHeight; set=>this._desiredHeight = value;}
    private float _contactThreshold = 28f;
    public float contactThreshold { get=>this._contactThreshold; set=>this._contactThreshold = value;}

    private float _wallHitHealthDecreasePct = 0.3f;
    public float wallHitHealthDecreasePct { get=>this._wallHitHealthDecreasePct; set=>this._wallHitHealthDecreasePct = value;}
    private float _boostHealthDecreasePct = 0.09f;
    public float boostHealthDecreasePct { get=>this._boostHealthDecreasePct; set=>this._boostHealthDecreasePct = value;}
    private float _recoveryHealthIncreasePct = 0.23f;
    public float recoveryHealthIncreasePct { get=>this._recoveryHealthIncreasePct; set=>this._recoveryHealthIncreasePct = value;}

    private float _turnSpeed = 100;
    public float turnSpeed { get=>this._turnSpeed; set=>this._turnSpeed = value;}
    private float _driftMulti = 1.8f;
    public float driftMulti { get=>this._driftMulti; set=>this._driftMulti = value;}
}
