using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour {
    [System.NonSerialized] public float currentTime = 0f;

    public void ResetTime() {
        currentTime = 0f;
    }

    void Update() {
        currentTime += Time.deltaTime;
    }
}
