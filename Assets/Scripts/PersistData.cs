using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.TestTools;

public class TimeSplit {
    public int minutes;
    public int seconds;
    public int milliseconds;

    public TimeSplit(string time_value) {
        var timeSplit = time_value.Split(":");
        int.TryParse(timeSplit[0], out minutes);
        int.TryParse(timeSplit[1], out seconds);
        int.TryParse(timeSplit[2], out milliseconds);
    }

    public static bool operator < (TimeSplit x, TimeSplit y) {
        if (x.minutes < y.minutes) { return true; }
        else if (x.minutes == y.minutes) {
            if (x.seconds < y.seconds) { return true; }
            else if (x.seconds == y.seconds) {
                return x.milliseconds < y.milliseconds;
            }
        }
        return false;
    }

    public static bool operator > (TimeSplit x, TimeSplit y) {
        if (x.minutes > y.minutes) { return true; }
        else if (x.minutes == y.minutes) {
            if (x.seconds > y.seconds) { return true; }
            else if (x.seconds == y.seconds) {
                return x.milliseconds > y.milliseconds;
            }
        }
        return false;
    }
}


public class TrackTime {
    public string trackTime;

    public TrackTime(string track_time) {
        trackTime = track_time;
    }

    public static bool operator == (TrackTime x, TrackTime y) {
        return x.trackTime == y.trackTime;
    }

    public static bool operator != (TrackTime x, TrackTime y) {
        return x.trackTime != y.trackTime ;
    }

    public static bool operator < (TrackTime x, TrackTime y) {
        return new TimeSplit(x.trackTime) < new TimeSplit(y.trackTime);
    }

    public static bool operator > (TrackTime x, TrackTime y) {
        return new TimeSplit(x.trackTime) > new TimeSplit(y.trackTime);
    }

    public override bool Equals(object obj) {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return (TrackTime)obj == this;
    }

    public override int GetHashCode() {
        unchecked {
            // TODO: This is not a good idea, but I probably won't need to hash these
            TimeSplit timeSplit = new TimeSplit(trackTime);
            return timeSplit.minutes ^ timeSplit.seconds ^ timeSplit.milliseconds;
        }
    }
}

public class PersistData {
    public Dictionary<string, TrackTime> trackTimeDict;

    public PersistData() {
        trackTimeDict = new Dictionary<string, TrackTime>();
    }

    public PersistData(Dictionary<string, TrackTime> track_time_dict) {
        trackTimeDict = track_time_dict;
    }
}
