using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadController : PlayerInputIntfc {
    public float GetFixedDeltaTime() {
        return Time.fixedDeltaTime;
    }

    public float GetAxisRaw(string axisName) {
        return 0f;
    }

    public bool GetKey(UnityEngine.KeyCode key) {
        return false;
    }
}