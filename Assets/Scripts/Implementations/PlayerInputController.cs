using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputController : PlayerInputIntfc {
    public float GetFixedDeltaTime() {
        return Time.fixedDeltaTime;
    }

    public float GetAxisRaw(string axisName) {
        return Input.GetAxisRaw(axisName);
    }

    public bool GetKey(UnityEngine.KeyCode key) {
        return Input.GetKey(key);
    }
}