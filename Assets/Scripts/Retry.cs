using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Retry : MonoBehaviour {

    [System.NonSerialized] public CustomSceneManager sceneMgr;
    [System.NonSerialized] public bool quitTriggerred = false;

    public void Awake() {
        sceneMgr = GameObject.FindObjectOfType<CustomSceneManager>();
    }

    public void RetryBtn() {
        SceneManager.LoadScene(sceneMgr.trackScene);
    }

    public void MainMenuBtn() {
        SceneManager.LoadScene("MainMenu");
    }

    public void QuitBtn() {
        // I shouldn't have to use this boolean for testing, as there is an
        // event that gets fired when quit gets called, but it doesn't seem
        // to be triggering how I expect, so this is fine 
        quitTriggerred = true;
        Application.Quit();
    }
}