using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerUI : MonoBehaviour {
    public GameObject playerVehicleObj;
    public GameObject timerObj;
    public TextMeshProUGUI speedDisp;
    public TextMeshProUGUI lapProgDisp;
    public TextMeshProUGUI currentTimeDisp;

    [System.NonSerialized] public int countdownTimer = 3;
    public TextMeshProUGUI countDownDisp;
    public Image healthBar;
    public TextMeshProUGUI bestTime;

    [System.NonSerialized] public Vehicle playerVehicleComp;
    [System.NonSerialized] public Timer timerComp;

    private void Awake() {
        countDownDisp.gameObject.SetActive(false);
        timerComp = timerObj.GetComponent<Timer>();
        playerVehicleComp = playerVehicleObj.GetComponent<Vehicle>();
    }

    public void Init(Manager.GameMode mode) {
        switch (mode) {
            case Manager.GameMode.TimeAttack:
                countDownDisp.gameObject.SetActive(true);
                StartCoroutine(StartCountdown());
                bestTime.text = "Best Time: " + Utils.LoadBestTime(SceneManager.GetActiveScene().name);
            break;
        }
    }

    void Update() {
        speedDisp.text = MakeUIContent.MakeSpeedometerString(playerVehicleComp.vehRigidBody.velocity.magnitude);
        healthBar.fillAmount = MakeUIContent.UpdateHealth(playerVehicleComp.currentHealth, playerVehicleComp.maxHealth);
        lapProgDisp.text = MakeUIContent.MakeLapString(playerVehicleComp.vehMgr.currentLapNum, playerVehicleComp.vehMgr.maxLaps);
        currentTimeDisp.text = MakeUIContent.MakeCurrentTime(timerComp.currentTime);
    }

    IEnumerator StartCountdown() {
        playerVehicleComp.enabled = false;
        timerComp.enabled = false;
        while (countdownTimer > 0) {
            countDownDisp.text = countdownTimer.ToString();
            yield return new WaitForSeconds(1f);
            countdownTimer--;
        }
        countDownDisp.text = "GO!";
        yield return new WaitForSeconds(0.2f);
        countDownDisp.gameObject.SetActive(false);
        // Fire off event here "unlocking" the game instead of referencing objects maybe...
        playerVehicleComp.enabled = true;
        timerComp.ResetTime();
        timerComp.enabled = true;
    }
}