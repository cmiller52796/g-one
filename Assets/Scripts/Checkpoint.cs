using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Checkpoint : MonoBehaviour {
    [System.NonSerialized] public int totalNumOfCheckpoints;
    public int idx;
    public static event Action checkObjective;

    void Start() {
        totalNumOfCheckpoints = GameObject.FindGameObjectsWithTag("Checkpoint").Length;
        var objName = this.gameObject.name;
        // Cheeky way to prevent me from having to index the checkpoints by hand
        if (objName == "Checkpoint") {
            idx = 1;
        }
        else {
            var nameSplit = objName.Split("(");
            int.TryParse(nameSplit[nameSplit.Length -1].Split(")")[0], out idx);
            idx++;
        }
    }

    private void OnTriggerEnter(Collider collision) {
        Vehicle vehicle = collision.gameObject.GetComponent<Vehicle>();
        if (vehicle) {
            if (vehicle.vehMgr.currentCheckPointIdx == idx - 1) {
                vehicle.vehMgr.currentCheckPointIdx = idx;
            }
            else if (idx == 1 && vehicle.vehMgr.currentCheckPointIdx == totalNumOfCheckpoints) {
                vehicle.vehMgr.currentCheckPointIdx = idx;
                vehicle.vehMgr.currentLapNum++;
                if (collision.gameObject.tag == "Player") {
                    FireCheckObjective();
                }
            }
        }
    }
    
    public void FireCheckObjective() {
        // Check if exists then call
        checkObjective?.Invoke();
    }
}