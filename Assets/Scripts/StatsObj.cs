using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsObj : MonoBehaviour, VehStatsIntfc {
    private float _normalMaxSpeed = 0f;
    public float normalMaxSpeed { get=> _normalMaxSpeed; set=>_normalMaxSpeed = value;}

    private float _boostAddSpeed = 0f;
    public float boostAddSpeed { get=>_boostAddSpeed; set=>_boostAddSpeed = value;}

    private float _normalAccel = 0f;
    public float normalAccel { get=>_normalAccel; set=>_normalAccel = value;}
    private float _normalDecel = 0f;
    public float normalDecel { get=>_normalDecel; set=>_normalDecel = value;}
    private float _boostDecel = 0f;
    public float boostDecel { get=>_boostDecel; set=>_boostDecel = value;}
    private float _boostAddAccel = 0f;
    public float boostAddAccel { get=>_boostAddAccel; set=>_boostAddAccel = value;}

    private float _desiredHeight = 0f;
    public float desiredHeight { get=>_desiredHeight; set=>_desiredHeight = value;}
    private float _contactThreshold = 0f;
    public float contactThreshold { get=>_contactThreshold; set=>_contactThreshold = value;}

    private float _wallHitHealthDecreasePct = 0f;
    public float wallHitHealthDecreasePct { get=>_wallHitHealthDecreasePct; set=>_wallHitHealthDecreasePct = value;}
    private float _boostHealthDecreasePct = 0f;
    public float boostHealthDecreasePct { get=>_boostHealthDecreasePct; set=>_boostHealthDecreasePct = value;}
    private float _recoveryHealthIncreasePct = 0f;
    public float recoveryHealthIncreasePct { get=>_recoveryHealthIncreasePct; set=>_recoveryHealthIncreasePct = value;}

    private float _turnSpeed = 0;
    public float turnSpeed { get=>_turnSpeed; set=>_turnSpeed = value;}
    private float _driftMulti = 0f;
    public float driftMulti { get=>_driftMulti; set=>_driftMulti = value;}
}
