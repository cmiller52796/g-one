using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VehicleCalcs {
    public static float CalcSpeedThrustOn(float velocity_delta, float max_speed, float current_speed) {
        // Ensure we don't exceed max speed from natural acceleration, 
        current_speed += (current_speed >= max_speed) ? 0f : velocity_delta;
        // If we have exceeded it from a boost, decelerate back to max speed
        if (current_speed > max_speed) {
                current_speed -= ((current_speed - velocity_delta) >= max_speed) ? velocity_delta : current_speed - max_speed;
            }
        return current_speed;
    }

    public static float CalcSpeedThrustOff(float velocity_delta, float current_speed) {
        current_speed -= (current_speed > 0) ? velocity_delta : 0;
        // Ensure we don't get a negative velocity
        if (current_speed < 0) {current_speed = 0;}
        return current_speed;
    }

    public static float CalcVelocityDelta(float acceleration, float fixed_delta_time) {
        // Accleration in units/s^2 * s gives velocity in units/s...
        return acceleration * fixed_delta_time;
    }

    public static Quaternion CalcTurn(float turn_amount, float turn_speed, bool drift_active, float drift_multi, float fixed_delta_time) {
        if (drift_active) {
            var vecComponent = turn_amount * turn_speed * drift_multi;
            return Quaternion.Euler(new Vector3 (0, vecComponent, -vecComponent) * fixed_delta_time);
        }
        else {
            return Quaternion.Euler(new Vector3 (0, turn_amount * turn_speed, 0) * fixed_delta_time);
        }
    }

    public static float CalcHeightAdjust(float height_over_track, float desired_height, float height_adjust_rate, float fixed_delta_time) {
        return Mathf.Lerp(height_over_track, desired_height, height_adjust_rate * fixed_delta_time) - height_over_track;
    }

    public static float CalcHealthDelta(float pct_of_max_health, float max_health, float fixed_delta_time) {
        return pct_of_max_health * max_health * fixed_delta_time;
    }

    // Could eliminate some copy paste here if we can the pct value being less than 0 or not, but it's marginal
    public static float CalcHealthLoss(float current_health, float health_delta, float min_health) {
        var new_current_health = current_health - health_delta;
        return (new_current_health >= min_health) ? new_current_health : min_health;
    }

    public static float CalcHealthGain(float current_health, float health_delta, float max_health) {
        var new_current_health = current_health + health_delta;
        return (new_current_health <= max_health) ? new_current_health : max_health;
    }

    public static bool CalcBoostValid(bool boost_on, float current_health, float min_boost_health) {
        if (boost_on && current_health > min_boost_health) {
            if (current_health > min_boost_health) {
                return true;
            }
        }
        return false;
    }

}