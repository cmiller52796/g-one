using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CalcBaryCoord {

    public static Vector3 GetSurfaceNormal(RaycastHit hit) {
        MeshCollider meshCollider = hit.collider as MeshCollider;
        if (meshCollider == null || meshCollider.sharedMesh == null)
        {
            // Should only get called in event a mesh is configured incorrectly
            return Vector3.up;
        }
        Mesh mesh = meshCollider.sharedMesh;
        return CalculateInterpolatedNormal(mesh, hit);
    }

    public static Vector3 CalculateInterpolatedNormal(Mesh mesh, RaycastHit hit) {
        // Grab all normals from mesh as collection Vector3s
        Vector3[] normals = mesh.normals;
        // Grab all triangles from mesh as int array (index 0-2 is the first triangle's xyz, 3-5 the second xyz, etc)
        int[] triangles = mesh.triangles;

        // The *3 is ensure you are skipping to the correct triangle, see comment about mesh.triangles above
        // Upon grabbing the triangle indices, plug into normals to get the coordinates of the 3 normals of the triangle
        // Note that these normals are in terms of local space!
        Vector3 n0 = normals[triangles[hit.triangleIndex * 3 + 0]];
        Vector3 n1 = normals[triangles[hit.triangleIndex * 3 + 1]];
        Vector3 n2 = normals[triangles[hit.triangleIndex * 3 + 2]];

        // Grab barycentric coordinate of the hitpoint, also in local space
        Vector3 baryCenter = hit.barycentricCoordinate;

        // Use barycentric coordinate to interpolate normal vector by using its coordinate values to
        // scale the normals as needed
        Vector3 interpolatedNormal = n0 * baryCenter.x + n1 * baryCenter.y + n2 * baryCenter.z;
        // Normalize the interpolated normal since we are just using the 
        // direction of this vector to align ourselves
        interpolatedNormal = interpolatedNormal.normalized;

        // Transform local space normals to world space 
        Transform hitTransform = hit.collider.transform;
        interpolatedNormal = hitTransform.TransformDirection(interpolatedNormal);
        return interpolatedNormal;
    }
}
