using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils {
    // Principle from: https://answers.unity.com/questions/950927/compare-vector3.html
    // Quaternion/Vector comparisons use floating point math which obviously has problems
    // when it comes to comparing exactness
    public static bool QuaternionCompare(Quaternion first, Quaternion second, float allowed_diff) {
        var dx = first.x - second.x;
        if (Mathf.Abs(dx) > allowed_diff)
            return false;
        var dy = first.y - second.y;
        if (Mathf.Abs(dy) > allowed_diff)
            return false;
        var dz = first.z - second.z;
        if (Mathf.Abs(dz) > allowed_diff)
            return false;
        var dw = first.w - second.w;
        if (Mathf.Abs(dw) > allowed_diff)
            return false;
        return true;
    }

    public static bool Vector3Compare(Vector3 first, Vector3 second, float allowed_diff) {
        var dx = first.x - second.x;
        if (Mathf.Abs(dx) > allowed_diff)
            return false;
        var dy = first.y - second.y;
        if (Mathf.Abs(dy) > allowed_diff)
            return false;
        var dz = first.z - second.z;
        if (Mathf.Abs(dz) > allowed_diff)
            return false;
        return true;
    }

    // Lerp until a certain level of difference, then just go straight to the value
    public static Vector3 Vector3LimitLerp(Vector3 start, Vector3 finish, float allowed_diff, float lerp_point) {
        if (Utils.Vector3Compare(start, finish, allowed_diff))
            return finish;
        return Vector3.Lerp(start, finish, lerp_point);
    }

    public static string TimeFloatToString(float time_seconds) {
        return TimeSpan.FromSeconds(time_seconds).ToString(@"mm\:ss\:fff");
    }

    public static bool UpdateBestTime(string track_name, float time_achieved) {
        TrackTime newTime = new TrackTime(Utils.TimeFloatToString(time_achieved));
        PersistData oldData = DataMgr.LoadData();
        // If the track is new or if the time is better than the existing one, save it
        if (!oldData.trackTimeDict.ContainsKey(track_name) || newTime < oldData.trackTimeDict[track_name]) {
            oldData.trackTimeDict[track_name] = newTime;
            DataMgr.SaveData(oldData);
            return true;
        }
        return false;
    }

    public static string LoadBestTime(string track_name) {
        PersistData oldData = DataMgr.LoadData();
        // If the track is new we need to add it to the existing dictionary and save it
        if (!oldData.trackTimeDict.ContainsKey(track_name)) {
            return "00:00:000";
        }
        return oldData.trackTimeDict[track_name].trackTime;
    }
}
