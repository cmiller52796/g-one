using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MakeUIContent {
    public static string MakeSpeedometerString(float velocity_value) {
        return ((int)velocity_value).ToString() + " m/s";
    }

    public static float UpdateHealth(float current_health, float max_health) {
        return current_health / max_health;
    }

    public static string MakeLapString(float current_lap, float max_laps) {
        return "Lap: " + current_lap.ToString() + "/" + max_laps.ToString();
    }

    public static string MakeCurrentTime(float time_seconds) {
        return "Time: " + Utils.TimeFloatToString(time_seconds);
    }
}