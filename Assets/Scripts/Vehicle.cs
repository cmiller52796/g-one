using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Vehicle : MonoBehaviour {
    // Values not vehicle specific--------------------------
    [System.NonSerialized] public float maxHealth = 100;
    [System.NonSerialized] public float minHealth = 0;
    [System.NonSerialized] public float minBoostHealth = 1f;
    // Change these at your own peril!
    [System.NonSerialized] public float heightAdjustRate = 100f;
    [System.NonSerialized] public float trackRotateAdjust = 20f;
    [System.NonSerialized] public float freeFallRotateAdjust = 10f;
    [System.NonSerialized] public float freeFallRate = 5f;
    // ------------------------------------------------------

    // Used more like globals in the class-------------------
    [System.NonSerialized] public float currentMaxSpeed;
    [System.NonSerialized] public float currentAccel;
    [System.NonSerialized] public float currentDecel;
    [System.NonSerialized] public float currentHealth;
    [System.NonSerialized] public float currentHeightOverTrack;
    // ------------------------------------------------------

    // Status vars-------------------------------------------
    [System.NonSerialized] public bool onSurface;
    [System.NonSerialized] public int wallHitTick;
    [System.NonSerialized] public int boostTick;
    [System.NonSerialized] public int recoveryTick;
    // ------------------------------------------------------

    // Inputs------------------------------------------------
    [System.NonSerialized] public bool thrusterInpActive;
    [System.NonSerialized] public bool driftInpActive;
    [System.NonSerialized] public bool boostInpActive;
    [System.NonSerialized] public bool boostValid;
    [System.NonSerialized] public float turnInput;
    // ------------------------------------------------------


    public Rigidbody vehRigidBody;
    public VehicleMgr vehMgr;
    public VehStatsIntfc vehStats;
    public PlayerInputIntfc input;

    public static event Action NoHealth;

    void Awake() {
        if (input == null) { input = new PlayerInputController(); }
        // Would need to fix this hard linkage if we need additional vehicles
        vehStats = new BlueFalconStats();
        currentHealth = maxHealth;
        currentAccel = vehStats.normalAccel;
        currentDecel = vehStats.normalDecel;
        currentMaxSpeed = vehStats.normalMaxSpeed;
        onSurface = false;
        wallHitTick = 0;
        boostTick = 0;
        recoveryTick = 0;
        thrusterInpActive = false;
        driftInpActive = false;
        boostInpActive = false;
        boostValid = false;
        turnInput = 0f;
    }

    void OnEnable() {
        VehicleMgr.objectiveDone += KillControlInput;
    }

    void OnDisable() {
        VehicleMgr.objectiveDone -= KillControlInput;
    }

    // FixedUpdate is called on fixed intervals, seems to be leading to more consistent physics behavior
    void FixedUpdate() {
        // Take in inputs
        thrusterInpActive = input.GetKey(KeyCode.Space);
        driftInpActive = input.GetKey(KeyCode.E);
        boostInpActive = input.GetKey(KeyCode.Q);
        turnInput = input.GetAxisRaw("Horizontal");
        // Check if we can stay in/enter boost state 
        boostValid = VehicleCalcs.CalcBoostValid(boostInpActive, currentHealth, minBoostHealth);
        Boost(boostValid);
        if (thrusterInpActive) {
            Accelerate(currentMaxSpeed, input.GetFixedDeltaTime());
        }
        else {
            Deccelerate(currentMaxSpeed, input.GetFixedDeltaTime());
        }
        RotateShip(turnInput, driftInpActive, input.GetFixedDeltaTime());
        Orient(input.GetFixedDeltaTime());
    }

    void OnCollisionEnter(Collision collision_info) {
        if (collision_info.collider.tag == "Track") { TakeWallDamage(); }
    }

    void OnTriggerEnter(Collider collider_info) {
        if (collider_info.tag == "Recovery") { Recover(); }
    }

    void OnTriggerStay(Collider collider_info) {
        if (collider_info.tag == "Recovery") { Recover(); }
    }

    void Boost(bool boost_valid) {
        if (!boost_valid) {
            currentMaxSpeed = vehStats.normalMaxSpeed;
            currentAccel = vehStats.normalAccel;
            currentDecel = vehStats.normalDecel;
            return;
        }
        boostTick++;
        currentMaxSpeed = vehStats.normalMaxSpeed + vehStats.boostAddSpeed;
        currentAccel = vehStats.normalAccel + vehStats.boostAddAccel;
        currentDecel = vehStats.boostDecel;
        LoseHealth(vehStats.boostHealthDecreasePct, minBoostHealth);
    }

    void TakeWallDamage() {
        wallHitTick++;
        LoseHealth(vehStats.wallHitHealthDecreasePct, minHealth);
    }

    void LoseHealth(float decrease_pct, float stop_loss) {
        currentHealth = VehicleCalcs.CalcHealthLoss(currentHealth, 
                                                    VehicleCalcs.CalcHealthDelta(decrease_pct, 
                                                                                maxHealth, 
                                                                                input.GetFixedDeltaTime()), 
                                                    stop_loss);
        if (currentHealth == minHealth) { 
            KillControlInput();
            FireNoHealth();
        }
    }

    void Recover() {
        recoveryTick++;
        currentHealth = VehicleCalcs.CalcHealthGain(currentHealth, 
                                                    VehicleCalcs.CalcHealthDelta(vehStats.recoveryHealthIncreasePct, 
                                                                                maxHealth, 
                                                                                input.GetFixedDeltaTime()), 
                                                    maxHealth);
    }

    void Accelerate(float current_max_speed, float fixed_delta_time) {
        vehRigidBody.velocity = transform.forward * VehicleCalcs.CalcSpeedThrustOn(VehicleCalcs.CalcVelocityDelta(currentAccel, fixed_delta_time), 
                                                                                    current_max_speed, vehRigidBody.velocity.magnitude);
    }

    void Deccelerate(float current_max_speed, float fixed_delta_time) {
        vehRigidBody.velocity = transform.forward * VehicleCalcs.CalcSpeedThrustOff(VehicleCalcs.CalcVelocityDelta(currentDecel, fixed_delta_time), 
                                                                                    vehRigidBody.velocity.magnitude);
    }

    void RotateShip(float turn_amount, bool drift_active, float fixed_delta_time) {
        // Check which way we are turning, if at all, and rotate (or not) in said direction, if drift is active, tilt the ship too.
        vehRigidBody.MoveRotation(vehRigidBody.rotation * VehicleCalcs.CalcTurn(turn_amount, vehStats.turnSpeed, drift_active, vehStats.driftMulti, fixed_delta_time));
    }

    void Orient(float fixed_delta_time) {
        var previousLocalUp = transform.up;
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -previousLocalUp, out hit, vehStats.contactThreshold) && hit.collider.tag != "DeathBarrier") {
            onSurface = true;

            Vector3 interpolatedNormal = CalcBaryCoord.GetSurfaceNormal(hit);
            var rotateTo = Utils.Vector3LimitLerp(previousLocalUp, interpolatedNormal, 0.01f, trackRotateAdjust * fixed_delta_time);
            // Use FromToRotation to get Quartenion difference between local y axis and the interpolated ray, then move the 
            // rotation of the rigidbody (the vehicle) to this new rotation via Quaternion multiplication
            vehRigidBody.MoveRotation(Quaternion.FromToRotation(previousLocalUp, rotateTo) * vehRigidBody.rotation);

            currentHeightOverTrack = hit.distance;
            AdjustHeight(previousLocalUp, currentHeightOverTrack, fixed_delta_time);
        }
        else {
            onSurface = false;
            // Interpolate between vector3s to the absolute up position to "right" ourselves when we fall
            var rotateTo = Utils.Vector3LimitLerp(previousLocalUp, Vector3.up, 0.01f, freeFallRotateAdjust * fixed_delta_time);
            vehRigidBody.MoveRotation(Quaternion.FromToRotation(previousLocalUp, rotateTo) * vehRigidBody.rotation);
            // For now we can just fall at a constant rate
            vehRigidBody.MovePosition(transform.localPosition + new Vector3(0, -freeFallRate, 0));
        }
    }

    // Ensure that the maximum height is gracefully set back down to for preventing abrupt jumps, 
    // especially since we are disabling gravity
    void AdjustHeight(Vector3 previous_local_up, float height_over_track, float fixed_delta_time) {
        // Basically if height over track is greater than desired height, result will be negative, 
        // otherwise positive, which is then applied to the upwards unit vector
        var heightAdjust = VehicleCalcs.CalcHeightAdjust(height_over_track, vehStats.desiredHeight, heightAdjustRate, fixed_delta_time);
        vehRigidBody.MovePosition(transform.localPosition + previous_local_up * heightAdjust);
    }

    public void FireNoHealth() {
        NoHealth?.Invoke();
    }

    void KillControlInput() {
        input = new DeadController();
    }
}
