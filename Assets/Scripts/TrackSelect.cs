using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TrackSelect : MonoBehaviour {

    [System.NonSerialized] public CustomSceneManager sceneMgr;

    public void Awake() {
        sceneMgr = GameObject.FindObjectOfType<CustomSceneManager>();
    }

    public void Track1() {
        SceneManager.LoadScene("Track1");
    }

    public void Track2() {
        SceneManager.LoadScene("Track2");
    }

    public void Track3() {
        SceneManager.LoadScene("Track3");
    }
}