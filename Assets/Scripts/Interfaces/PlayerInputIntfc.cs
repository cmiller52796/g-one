using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Abstraction layer for inputs
public interface PlayerInputIntfc {
    float GetFixedDeltaTime();
    float GetAxisRaw(string axisName);
    bool GetKey(UnityEngine.KeyCode key);
}
