using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Abstraction layer for vehicle statistics
public interface VehStatsIntfc {
    float normalMaxSpeed { get; set;}
    float boostAddSpeed { get; set;}

    float normalAccel { get; set;}
    float normalDecel { get; set;}
    float boostDecel { get; set;}
    float boostAddAccel { get; set;}

    float desiredHeight { get; set;}
    float contactThreshold { get; set;}

    float wallHitHealthDecreasePct { get; set;}
    float boostHealthDecreasePct { get; set;}
    float recoveryHealthIncreasePct { get; set;}

    float turnSpeed { get; set;}
    float driftMulti { get; set;}
}