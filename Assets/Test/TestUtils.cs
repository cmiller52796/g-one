using System.Collections;
using System.Collections.Generic;
using NSubstitute;
using UnityEngine;

public static class TestUtils {
    public static (Vehicle, PlayerInputIntfc) init() {
        GameObject vehicleObj = GameObject.Find("BlueFalcon");
        vehicleObj.tag = "Player";
        Vehicle vehicleComp = vehicleObj.GetComponent<Vehicle>();
        // Set vehicle position and rotation to zeroed out values
        reset_position(ref vehicleComp);
        // Setup mock input controller
        var inputMock = Substitute.For<PlayerInputIntfc>();
        // Use the real fixed delta time
        inputMock.GetFixedDeltaTime().Returns(Time.fixedDeltaTime);
        vehicleComp.input = inputMock;
        return (vehicleComp, inputMock);
    }

    public static void reset_position(ref Vehicle vehicle_comp) {
        vehicle_comp.transform.position = new Vector3(0, 0, 0);
        vehicle_comp.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
    }

    public static Vector3 GetSurfaceNormalFromObj(Transform obj_transform) {
        RaycastHit hit;
        Vector3 interpolatedNormal = new Vector3(0, 0, 0);
        if (Physics.Raycast(obj_transform.position, -obj_transform.up, out hit)) {
            interpolatedNormal = CalcBaryCoord.GetSurfaceNormal(hit);
        }
        return interpolatedNormal;
    }

    public static float GetExpectedHealthLoss(float health_dec_pct, float max_health, float min_health, int tick, float fixed_delta_time) {
        var healthDelta = VehicleCalcs.CalcHealthDelta(health_dec_pct, max_health, fixed_delta_time);
        var calcHealth = max_health - healthDelta * tick;
        return calcHealth >= min_health ? calcHealth : min_health;
    }

    public static float GetExpectedHealthGain(float health_inc_pct, float max_health, float min_health, float starting_health, int tick, float fixed_delta_time) {
        var healthDelta = VehicleCalcs.CalcHealthDelta(health_inc_pct, max_health, fixed_delta_time);
        var calcHealth = starting_health + healthDelta * tick;
        return calcHealth <= max_health ? calcHealth : max_health;
    }

    public delegate bool ConditionCB();
    public static IEnumerator WaitWhile(ConditionCB condition_cb, int timeout=20) {
        // Run this first iteration to ensure we actually "run" before checking our condition
        yield return new WaitForSeconds(1);
        int counter = 0;
        counter++;
        while (condition_cb() && counter < timeout) {
            yield return new WaitForSeconds(1);
            counter++;
        }
    }
}