using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;
using UnityEngine;
using UnityEngine.TestTools;
using TMPro;

namespace Tests {
    public class TimerTests {
        [SetUp]
        public void SetUp() {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Tests_Objects");
        }

        [UnityTest]
        public IEnumerator TestTimer() {
            GameObject timerObj = GameObject.Find("Timer");
            Timer timerComp = timerObj.GetComponent<Timer>();
            float timeToWait = 1f;
            yield return new WaitForSeconds(timeToWait);
            Assert.Greater(timerComp.currentTime, 0f);
            Assert.Less(timerComp.currentTime, timeToWait + 1f);
        }

        [UnityTest]
        public IEnumerator TestResetTime() {
            GameObject timerObj = GameObject.Find("Timer");
            Timer timerComp = timerObj.GetComponent<Timer>();
            float timeToWait = 1f;
            yield return new WaitForSeconds(timeToWait);
            Assert.Greater(timerComp.currentTime, 0f);
            Assert.Less(timerComp.currentTime, timeToWait + 1f);
            timerComp.ResetTime();
            Assert.AreEqual(timerComp.currentTime, 0f, 0.1f);
        }
    }
}
