using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;
using UnityEngine;
using UnityEngine.TestTools;
using TMPro;

namespace Tests {
    public class ManagerTests {
        [SetUp]
        public void SetUp() {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Tests_Objects");
        }

        [UnityTest]
        public IEnumerator TestManagerInit() {
            GameObject mgrObj = GameObject.Find("Manager");
            Manager mgrComp = mgrObj.GetComponent<Manager>();
            /*
            bool callbackTriggered = false;
            Manager.GameMode cbMode = Manager.GameMode.Default;
            void temp_cb(Manager.GameMode mode) {
                callbackTriggered = true;
                cbMode = mode;
            }
            mgrComp.callback = temp_cb;
            */
            // Get past start
            yield return new WaitForFixedUpdate();
            // Check for frame rate limit
            Assert.AreEqual(Application.targetFrameRate, mgrComp.setFrameRate);
            /* TODO: Make sure initial actions were done, this is commented out because
            // for some reason initial values aren't being reserved once the test starts
            Assert.True(callbackTriggered);
            Assert.AreEqual(mgrComp.mode, cbMode);
            */
            // Setup check for check objective event
            GameObject vehMgrObj = GameObject.Find("VehMgr");
            VehicleMgr vehMgrComp = vehMgrObj.GetComponent<VehicleMgr>();
            vehMgrComp.FireObjectiveDone();
            // Assure our event trigger was setup
            yield return new WaitForFixedUpdate();
            Assert.True(mgrComp.objectiveComplete);
        }
    }
}