using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;

namespace Tests {
    public class TrackSelectTests {
        [SetUp]
        public void SetUp() {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Tests_Track_Select");
        }

        public TrackSelect TestTrackSelectInit() {
            GameObject trackSelObj = GameObject.Find("TrackSelect");
            return trackSelObj.GetComponent<TrackSelect>();
        }

        [UnityTest]
        public IEnumerator TestTrackSelectTrack1() {
            TrackSelect trackSelComp = TestTrackSelectInit();
            // Get past start
            yield return new WaitForFixedUpdate();
            trackSelComp.Track1();
            yield return new WaitForSeconds(3f);
            Assert.True(SceneManager.GetSceneByName("Track1").isLoaded);
        }

        [UnityTest]
        public IEnumerator TestTrackSelectTrack2() {
            TrackSelect trackSelComp = TestTrackSelectInit();
            // Get past start
            yield return new WaitForFixedUpdate();
            trackSelComp.Track2();
            yield return new WaitForSeconds(3f);
            Assert.True(SceneManager.GetSceneByName("Track2").isLoaded);
        }

        [UnityTest]
        public IEnumerator TestTrackSelectTrack3() {
            TrackSelect trackSelComp = TestTrackSelectInit();
            // Get past start
            yield return new WaitForFixedUpdate();
            trackSelComp.Track3();
            yield return new WaitForSeconds(3f);
            Assert.True(SceneManager.GetSceneByName("Track3").isLoaded);
        }
    }
}