using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;

namespace Tests {
    public class AudioManagerTests {
        [SetUp]
        public void SetUp() {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Tests_Audio_Manager");
        }

        [UnityTest]
        public IEnumerator TestAudioManager() {
            AudioManager mgrComp = GameObject.Find("AudioManager").GetComponent<AudioManager>();
            yield return new WaitForSeconds(1f);

            UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
            yield return new WaitForSeconds(3f);
            Assert.True(mgrComp.audioSource.isPlaying);
            Assert.AreEqual(mgrComp.audioSource.clip.name, mgrComp.trackMusicDict["MainMenu"].name);

            UnityEngine.SceneManagement.SceneManager.LoadScene("TrackSelect");
            yield return new WaitForSeconds(3f);
            Assert.True(mgrComp.audioSource.isPlaying);
            Assert.AreEqual(mgrComp.audioSource.clip.name, mgrComp.trackMusicDict["MainMenu"].name);

            UnityEngine.SceneManagement.SceneManager.LoadScene("Track1");
            yield return new WaitForSeconds(3f);
            Assert.True(mgrComp.audioSource.isPlaying);
            Assert.AreEqual(mgrComp.audioSource.clip.name, mgrComp.trackMusicDict["Track1"].name);

            UnityEngine.SceneManagement.SceneManager.LoadScene("Track2");
            yield return new WaitForSeconds(3f);
            Assert.True(mgrComp.audioSource.isPlaying);
            Assert.AreEqual(mgrComp.audioSource.clip.name, mgrComp.trackMusicDict["Track2"].name);

            UnityEngine.SceneManagement.SceneManager.LoadScene("Track3");
            yield return new WaitForSeconds(3f);
            Assert.True(mgrComp.audioSource.isPlaying);
            Assert.AreEqual(mgrComp.audioSource.clip.name, mgrComp.trackMusicDict["Track3"].name);
        }
    }
}