using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;

namespace Tests {
    public class MainMenuTests {
        [SetUp]
        public void SetUp() {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Tests_Menu");
        }

        public MainMenu TestMenuInit() {
            GameObject menuObj = GameObject.Find("MainMenu");
            return menuObj.GetComponent<MainMenu>();
        }

        [UnityTest]
        public IEnumerator TestMainMenuAwake() {
            MainMenu menuComp = TestMenuInit();
            // Get past start
            yield return new WaitForFixedUpdate();
            Assert.AreEqual(Application.targetFrameRate, menuComp.setFrameRate);
        }

        [UnityTest]
        public IEnumerator TestMenuTimeAttack() {
            MainMenu menuComp = TestMenuInit();
            // Get past start
            yield return new WaitForFixedUpdate();
            menuComp.TimeAttack();
            yield return new WaitForSeconds(3f);
            Assert.True(SceneManager.GetSceneByName("TrackSelect").isLoaded);
            Assert.AreEqual(Manager.GameMode.TimeAttack, menuComp.sceneMgr.mode);
        }

        [UnityTest]
        public IEnumerator TestMenuQuit() {
            MainMenu menuComp = TestMenuInit();
            // Get past start
            yield return new WaitForFixedUpdate();
            menuComp.QuitGame();
            yield return new WaitForSeconds(1f);
            // I shouldn't have to use this boolean for testing, as there is an
            // event that gets fired when quit gets called and you can just use your 
            // own callback, but it doesn't seem to be triggering how I expect, 
            // so this is fine for now
            Assert.True(menuComp.quitTriggerred);
        }
    }
}