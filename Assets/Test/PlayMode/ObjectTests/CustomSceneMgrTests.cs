using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    public class CustomSceneMgrTests {
        [SetUp]
        public void SetUp() {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Tests_Custom_Scene_Manager");
        }

        [UnityTest]
        public IEnumerator TestCustomSceneManagerAwake() {
            GameObject mgrObj = GameObject.Find("CustomSceneManager");
            CustomSceneManager mgrComp = mgrObj.GetComponent<CustomSceneManager>();
            // Get past start
            yield return new WaitForFixedUpdate();
            Assert.AreEqual("Tests_Custom_Scene_Manager", mgrComp.currentScene);
        }

        [UnityTest]
        public IEnumerator TestCustomSceneManagerLoading() {
            GameObject mgrObj = GameObject.Find("CustomSceneManager");
            CustomSceneManager mgrComp = mgrObj.GetComponent<CustomSceneManager>();
            // Get past start
            yield return new WaitForSeconds(2f);
            Assert.AreEqual("Tests_Custom_Scene_Manager", mgrComp.currentScene);
            UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
            yield return new WaitForSeconds(2f);
            Assert.AreEqual("MainMenu", mgrComp.currentScene);
            UnityEngine.SceneManagement.SceneManager.LoadScene("Track1");
            yield return new WaitForSeconds(2f);
            Assert.AreEqual("Track1", mgrComp.currentScene);
            Assert.NotNull(mgrComp.mgr);
        }
    }
}