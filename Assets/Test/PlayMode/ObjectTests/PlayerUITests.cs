using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;
using UnityEngine;
using UnityEngine.TestTools;
using TMPro;

namespace Tests {
    public class PlayerUITests {
        [SetUp]
        public void SetUp() {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Tests_Objects");
        }

        [UnityTest]
        public IEnumerator TestPlayerUIAwake() {
            PlayerUI playerUIObj = GameObject.FindObjectOfType<PlayerUI>();
            yield return new WaitForFixedUpdate();
            Assert.False(playerUIObj.countDownDisp.IsActive());
        }

        [UnityTest]
        public IEnumerator TestPlayerUIInitTimeAtkMode() {
            GameObject playerUIObj = GameObject.Find("PlayerUI");
            PlayerUI playerUIComp = playerUIObj.GetComponent<PlayerUI>();
            playerUIComp.Init(Manager.GameMode.TimeAttack);
            bool condition_countdown_active() {
                return playerUIComp.countDownDisp.isActiveAndEnabled;
            }
            yield return TestUtils.WaitWhile(condition_countdown_active, playerUIComp.countdownTimer+1);
            Assert.False(playerUIComp.countDownDisp.isActiveAndEnabled);
            Assert.True(playerUIComp.playerVehicleComp.isActiveAndEnabled);
            Assert.True(playerUIComp.timerComp.isActiveAndEnabled);
        }

        [UnityTest]
        public IEnumerator TestPlayerUIUpdate() {
            PlayerUI playerUIObj = GameObject.FindObjectOfType<PlayerUI>();
            yield return new WaitForFixedUpdate();
            Assert.AreEqual(playerUIObj.speedDisp.text, MakeUIContent.MakeSpeedometerString(playerUIObj.playerVehicleComp.vehRigidBody.velocity.magnitude));
            Assert.AreEqual(playerUIObj.healthBar.fillAmount, MakeUIContent.UpdateHealth(playerUIObj.playerVehicleComp.currentHealth, playerUIObj.playerVehicleComp.maxHealth));
            Assert.AreEqual(playerUIObj.lapProgDisp.text, MakeUIContent.MakeLapString(playerUIObj.playerVehicleComp.vehMgr.currentLapNum, playerUIObj.playerVehicleComp.vehMgr.maxLaps));
        }
    }
}