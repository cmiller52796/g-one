using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;

namespace Tests {
    public class RetryTests {
        [SetUp]
        public void SetUp() {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Tests_Retry");
        }

        public Retry TestRetryInit() {
            GameObject retryObj = GameObject.Find("Retry");
            return retryObj.GetComponent<Retry>();
        }

        [UnityTest]
        public IEnumerator TestRetryBtn() {
            Retry retryComp = TestRetryInit();
            retryComp.sceneMgr.trackScene = "Track1";
            // Get past start
            yield return new WaitForFixedUpdate();
            retryComp.RetryBtn();
            yield return new WaitForSeconds(3f);
            Assert.True(SceneManager.GetSceneByName("Track1").isLoaded);
        }

        [UnityTest]
        public IEnumerator TestMainMenuBtn() {
            Retry retryComp = TestRetryInit();
            // Get past start
            yield return new WaitForFixedUpdate();
            retryComp.MainMenuBtn();
            yield return new WaitForSeconds(3f);
            Assert.True(SceneManager.GetSceneByName("MainMenu").isLoaded);
        }

        [UnityTest]
        public IEnumerator TestQuitBtn() {
            Retry retryComp = TestRetryInit();
            // Get past start
            yield return new WaitForFixedUpdate();
            retryComp.QuitBtn();
            yield return new WaitForSeconds(1f);
            Assert.True(retryComp.quitTriggerred);
        }

        [UnityTest]
        public IEnumerator TestRetryNoHealthTrigger() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Get past start
            yield return new WaitForFixedUpdate();
            // Simulate no health condition
            vehicleComp.FireNoHealth();
            yield return new WaitForSeconds(3f);
            Assert.True(SceneManager.GetSceneByName("RetryCrash").isLoaded);
        }

        [UnityTest]
        public IEnumerator TestRetryCompleteTrigger() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Get past start
            yield return new WaitForFixedUpdate();
            // Simulate finished objective conditions
            vehicleComp.vehMgr.FireObjectiveDone();
            yield return new WaitForSeconds(3f);
            Assert.True(SceneManager.GetSceneByName("RetryComplete").isLoaded);
        }
    }
}