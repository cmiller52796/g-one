using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;
using UnityEngine;
using UnityEngine.TestTools;
using TMPro;

namespace Tests {
    public class VehicleMgrTests {
        [SetUp]
        public void SetUp() {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Tests_Objects");
        }

        [UnityTest]
        public IEnumerator TestVehMgrCheckObjTrigger() {
            GameObject vehMgrObj = GameObject.Find("VehMgr");
            VehicleMgr vehMgrComp = vehMgrObj.GetComponent<VehicleMgr>();
            bool callbackTriggered = false;
            void callback() {
                callbackTriggered = true;
            }
            VehicleMgr.objectiveDone += callback;
            GameObject checkpointObj = GameObject.Find("Checkpoint");
            Checkpoint checkpointComp = checkpointObj.GetComponent<Checkpoint>();
            // Get past start to subscribe
            yield return new WaitForFixedUpdate();
            // Set vehicle manager to pass its if statement in the event trigger
            vehMgrComp.currentLapNum = vehMgrComp.maxLaps + 1;
            checkpointComp.FireCheckObjective();
            yield return new WaitForFixedUpdate();
            Assert.True(callbackTriggered);
        }
    }
}
