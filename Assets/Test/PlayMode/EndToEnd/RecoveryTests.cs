using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    public class RecoveryTests {
        [SetUp]
        public void SetUp() {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Tests_Recovery");
        }

        [UnityTest]
        public IEnumerator TestRecoverySurface() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            inputMock.GetKey(KeyCode.Space).Returns(true);
            float startingHealth = 1f;
            vehicleComp.currentHealth = startingHealth;
            yield return new WaitForSeconds(1);
            inputMock.GetKey(KeyCode.Space).Returns(false);
            yield return new WaitForSeconds(1);
            var expected = TestUtils.GetExpectedHealthGain(vehicleComp.vehStats.recoveryHealthIncreasePct, 
                                                        vehicleComp.maxHealth, 
                                                        vehicleComp.minHealth, 
                                                        startingHealth,
                                                        vehicleComp.recoveryTick, 
                                                        Time.fixedDeltaTime);
            Assert.AreEqual(expected, vehicleComp.currentHealth, 0.1f);
        }

        [UnityTest]
        public IEnumerator TestRecoverySurfaceStay() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            vehicleComp.transform.position = new Vector3(-576, 0, 0);
            inputMock.GetKey(KeyCode.Space).Returns(true);
            float startingHealth = 1f;
            vehicleComp.currentHealth = startingHealth;
            yield return new WaitForSeconds(1);
            inputMock.GetKey(KeyCode.Space).Returns(false);
            yield return new WaitForSeconds(3);
            var expected = TestUtils.GetExpectedHealthGain(vehicleComp.vehStats.recoveryHealthIncreasePct, 
                                                        vehicleComp.maxHealth, 
                                                        vehicleComp.minHealth, 
                                                        startingHealth,
                                                        vehicleComp.recoveryTick, 
                                                        Time.fixedDeltaTime);
            Assert.AreEqual(expected, vehicleComp.currentHealth, 0.1f);
        }
    }
}
