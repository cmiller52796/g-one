using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;

namespace Tests {
    public class NoTrackTests {
        [SetUp]
        public void SetUp() {
            SceneManager.LoadScene("Tests_No_Track");
        }

        [UnityTest]
        public IEnumerator TestInputMapping() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Ensure that these input variables change when expected
            inputMock.GetKey(KeyCode.Space).Returns(true);
            inputMock.GetKey(KeyCode.E).Returns(true);
            inputMock.GetKey(KeyCode.Q).Returns(true);
            inputMock.GetAxisRaw("Horizontal").Returns(1f);
            yield return new WaitForSeconds(2);
            Assert.True(vehicleComp.thrusterInpActive);
            Assert.True(vehicleComp.driftInpActive);
            Assert.True(vehicleComp.boostInpActive);
            Assert.AreEqual(vehicleComp.turnInput, 1f, 0.1f);

            inputMock.GetKey(KeyCode.Space).Returns(false);
            inputMock.GetKey(KeyCode.E).Returns(false);
            inputMock.GetKey(KeyCode.Q).Returns(false);
            inputMock.GetAxisRaw("Horizontal").Returns(-1f);
            yield return new WaitForSeconds(2);
            Assert.False(vehicleComp.thrusterInpActive);
            Assert.False(vehicleComp.driftInpActive);
            Assert.False(vehicleComp.boostInpActive);
            Assert.AreEqual(vehicleComp.turnInput, -1f, 0.1f);
        }

        [UnityTest]
        public IEnumerator TestFallRotate() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            vehicleComp.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 90));
            yield return new WaitForSeconds(4);
            // Check that we have rotated to world up
            Assert.True(Utils.QuaternionCompare(Quaternion.Euler(new Vector3(0, 0, 0)), vehicleComp.transform.rotation, 0.01f));
            Assert.True(Utils.Vector3Compare(Vector3.up, vehicleComp.transform.up, 0.01f));
        }

        [UnityTest]
        public IEnumerator TestFallOnBarrier() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            bool condition_cb() { 
                return !SceneManager.GetSceneByName("RetryCrash").isLoaded;
            }
            yield return TestUtils.WaitWhile(condition_cb, 50);
            Assert.True(SceneManager.GetSceneByName("RetryCrash").isLoaded);
        }
    }
}
