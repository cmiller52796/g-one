using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    public class InvertSurfaceTests {
        [SetUp]
        public void SetUp() {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Tests_Invert");
        }

        [UnityTest]
        public IEnumerator TestVerticalMotion() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Accelerate
            inputMock.GetKey(KeyCode.Space).Returns(true);
            inputMock.GetAxisRaw("Horizontal").Returns(0.01f);
            yield return new WaitForSeconds(2);
            // Check that x coordinate hasn't changed, but z has in positive direction
            Assert.Positive(vehicleComp.transform.position.x);
            Assert.Positive(vehicleComp.transform.position.y);
            Assert.Positive(vehicleComp.transform.position.z);
            TestUtils.reset_position(ref vehicleComp);
            inputMock.GetAxisRaw("Horizontal").Returns(-0.01f);
            yield return new WaitForSeconds(2);
            Assert.Negative(vehicleComp.transform.position.x);
            Assert.Positive(vehicleComp.transform.position.y);
            Assert.Positive(vehicleComp.transform.position.z);
        }

        [UnityTest]
        public IEnumerator TestInvertAdhesion() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Accelerate until we go inverted, then come to stop
            inputMock.GetKey(KeyCode.Space).Returns(true);
            // Wait until we smack wall
            bool condition_cb() { return vehicleComp.wallHitTick == 0f;}
            yield return TestUtils.WaitWhile(condition_cb);
            // Test we are adhered before proceeding
            Assert.True(vehicleComp.onSurface);
            Vector3 surfaceNormal = TestUtils.GetSurfaceNormalFromObj(vehicleComp.transform);
            // If our vehicle's upwards vector matches the surface normal, we're oriented properly
            Assert.True(Utils.Vector3Compare(surfaceNormal, vehicleComp.transform.up, 0.01f));
            // Check to see we are upside down
            Assert.Negative(vehicleComp.transform.up.y);
        }
    }
}
