using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    public class CylinderSurfaceTests {
        [SetUp]
        public void SetUp() {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Tests_Cylinder");
        }

        [UnityTest]
        public IEnumerator TestCylinderWrap() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Accelerate and turn
            inputMock.GetKey(KeyCode.Space).Returns(true);
            inputMock.GetAxisRaw("Horizontal").Returns(0.5f);
            yield return new WaitForSeconds(0.5f);
            // Go straight, which should wrap us around
            inputMock.GetAxisRaw("Horizontal").Returns(0f);
            yield return new WaitForSeconds(6);
            // Turn the other way
            inputMock.GetAxisRaw("Horizontal").Returns(-0.5f);
            yield return new WaitForSeconds(1);
            inputMock.GetAxisRaw("Horizontal").Returns(0f);
            // Come to a stop
            inputMock.GetKey(KeyCode.Space).Returns(false);
            yield return new WaitForSeconds(4);
            // Check that we are still on the track and oriented properly
            Assert.True(vehicleComp.onSurface);
            Vector3 surfaceNormal = TestUtils.GetSurfaceNormalFromObj(vehicleComp.transform);
            // If our vehicle's upwards vector matches the surface normal, we're oriented correctly
            Assert.True(Utils.Vector3Compare(surfaceNormal, vehicleComp.transform.up, 0.01f));
        }

        [UnityTest]
        public IEnumerator TestFallRotateToCylinder() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Need to go high enough to avoid collision whilst falling
            vehicleComp.transform.position = new Vector3(400, 200, 0);
            yield return new WaitForSeconds(4);
            // Check that our current height above the track matches the desired height
            Assert.True(vehicleComp.onSurface);
            Vector3 surfaceNormal = TestUtils.GetSurfaceNormalFromObj(vehicleComp.transform);
            // If our vehicle's upwards vector matches the surface normal, we're oriented correctly
            Assert.True(Utils.Vector3Compare(surfaceNormal, vehicleComp.transform.up, 0.01f));
            Assert.AreEqual(vehicleComp.vehStats.desiredHeight, vehicleComp.currentHeightOverTrack, 0.01f);
        }
    }
}
