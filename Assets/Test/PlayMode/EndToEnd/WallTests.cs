using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    public class WallTests {
        [SetUp]
        public void SetUp() {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Tests_Walls");
        }

        [UnityTest]
        public IEnumerator TestContact() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Accelerate into the wall
            inputMock.GetKey(KeyCode.Space).Returns(true);
            yield return new WaitForSeconds(4);
            // Check that we smacked the wall
            Assert.GreaterOrEqual(vehicleComp.wallHitTick, 1);
            // Check that our health has reduced by the correct amount
            var expected = TestUtils.GetExpectedHealthLoss(vehicleComp.vehStats.wallHitHealthDecreasePct, 
                                                        vehicleComp.maxHealth, 
                                                        vehicleComp.minHealth, 
                                                        vehicleComp.wallHitTick, 
                                                        Time.fixedDeltaTime);
            Assert.AreEqual(expected, vehicleComp.currentHealth, 0.1f);
        }

        [UnityTest]
        public IEnumerator TestContactToNoHealth() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Speed things up a bit by making wall hits smack HARD
            vehicleComp.vehStats.wallHitHealthDecreasePct = 5f;
            // Accelerate into the wall
            inputMock.GetKey(KeyCode.Space).Returns(true);
            bool condition_cb() { 
                return vehicleComp.currentHealth != vehicleComp.minHealth;
            }
            bool event_called = false;
            void event_cb() { 
                event_called = true;
            }
            Vehicle.NoHealth += event_cb;
            yield return TestUtils.WaitWhile(condition_cb);
            // Check that we smacked the wall
            Assert.GreaterOrEqual(vehicleComp.wallHitTick, 1);
            // Check that our health is at minimum
            Assert.AreEqual(vehicleComp.currentHealth, vehicleComp.minHealth, 0.1f);
            // Check event fired off and controller set is the dead one
            Assert.True(event_called);
            Assert.True(vehicleComp.input is DeadController);
        }
    }
}
