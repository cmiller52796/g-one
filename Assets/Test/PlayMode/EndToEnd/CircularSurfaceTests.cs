using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    public class CircularSurfaceTests {
        [SetUp]
        public void SetUp() {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Tests_Circular");
        }

        [UnityTest]
        public IEnumerator TestCheckpointsEndLogic() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Accelerate around the loop, hitting the checkpoints until we reach our maximum laps
            inputMock.GetKey(KeyCode.Space).Returns(true);
            bool condition_cb() { 
                return vehicleComp.vehMgr.currentLapNum <= vehicleComp.vehMgr.maxLaps;
            }
            bool eventTriggered = false;
            void event_cb() {
                eventTriggered = true;
            }
            Checkpoint.checkObjective += event_cb;
            // Timeout of 30s (test should take around 22s)
            yield return TestUtils.WaitWhile(condition_cb, 30);
            Assert.AreEqual(vehicleComp.vehMgr.currentLapNum - 1, vehicleComp.vehMgr.maxLaps);
            Assert.AreEqual(vehicleComp.vehMgr.currentCheckPointIdx, 1);
            Assert.True(eventTriggered);
        }

        [UnityTest]
        public IEnumerator TestCheckpointSelfNumber() {
            yield return new WaitForSeconds(1);
            var allCheckPoints = GameObject.FindGameObjectsWithTag("Checkpoint");
            int testIdx = 0;
            foreach (var gameObject in allCheckPoints) {
                var objName = gameObject.name;
                Checkpoint chkPt = gameObject.GetComponent<Checkpoint>();
                if (objName == "Checkpoint") {
                    Assert.AreEqual(chkPt.idx, 1);
                }
                else {
                    var nameSplit = objName.Split("(");
                    int.TryParse(nameSplit[nameSplit.Length -1].Split(")")[0], out testIdx);
                    Assert.AreEqual(chkPt.idx, testIdx + 1);
                }
            }
        }
    }
}
