using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;

namespace Tests {
    public class FlatSurfaceTests {
        [SetUp]
        public void SetUp() {
            SceneManager.LoadScene("Tests_Flat");
        }

        [UnityTest]
        public IEnumerator TestAccel() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Accelerate
            inputMock.GetKey(KeyCode.Space).Returns(true);
            yield return new WaitForSeconds(2);
            // Check that x coordinate hasn't changed, but z has in positive direction
            Assert.AreEqual(0, vehicleComp.transform.position.x, 0.1f);
            Assert.AreNotEqual(0, vehicleComp.transform.position.z);
            Assert.Positive(vehicleComp.transform.position.z);
        }

        [UnityTest]
        public IEnumerator TestAccelRate() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Accelerate
            inputMock.GetKey(KeyCode.Space).Returns(true);
            yield return new WaitForSeconds(4);
            // Check that vehicle velocity is equal to max speed
            Assert.AreEqual(vehicleComp.vehStats.normalMaxSpeed, vehicleComp.vehRigidBody.velocity.magnitude, 0.1f);
        }

        [UnityTest]
        public IEnumerator TestMaxSpeed() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Accelerate
            inputMock.GetKey(KeyCode.Space).Returns(true);
            yield return new WaitForSeconds(10);
            // Check that vehicle velocity is equal to max speed
            Assert.AreEqual(vehicleComp.vehStats.normalMaxSpeed, vehicleComp.vehRigidBody.velocity.magnitude, 0.1f);
        }

        [UnityTest]
        public IEnumerator TestBoost() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Accelerate
            inputMock.GetKey(KeyCode.Space).Returns(true);
            inputMock.GetKey(KeyCode.Q).Returns(true);
            yield return new WaitForSeconds(5);
            // Check that vehicle velocity is equal to max speed + boost
            Assert.AreEqual(vehicleComp.vehStats.normalMaxSpeed + vehicleComp.vehStats.boostAddSpeed, vehicleComp.currentMaxSpeed, 0.1f);
            Assert.AreEqual(vehicleComp.vehStats.normalAccel + vehicleComp.vehStats.boostAddAccel, vehicleComp.currentAccel, 0.1f);
            Assert.AreEqual(vehicleComp.vehStats.boostDecel, vehicleComp.currentDecel, 0.1f);
            Assert.AreEqual(vehicleComp.currentMaxSpeed, vehicleComp.vehRigidBody.velocity.magnitude, 0.1f);
            var expected = TestUtils.GetExpectedHealthLoss(vehicleComp.vehStats.boostHealthDecreasePct, 
                                                        vehicleComp.maxHealth, 
                                                        vehicleComp.minBoostHealth, 
                                                        vehicleComp.boostTick, 
                                                        Time.fixedDeltaTime);
            Assert.AreEqual(expected, vehicleComp.currentHealth, 0.1f);
        }

        [UnityTest]
        public IEnumerator TestBoostToMinHealth() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Speed up the test
            vehicleComp.vehStats.boostHealthDecreasePct = 0.5f;
            // Accelerate
            inputMock.GetKey(KeyCode.Space).Returns(true);
            inputMock.GetKey(KeyCode.Q).Returns(true);
            yield return new WaitForSeconds(5);
            // Check that vehicle velocity is equal to normal max speed 
            Assert.AreEqual(vehicleComp.vehStats.normalMaxSpeed, vehicleComp.currentMaxSpeed, 0.1f);
            Assert.AreEqual(vehicleComp.vehStats.normalAccel, vehicleComp.currentAccel, 0.1f);
            Assert.AreEqual(vehicleComp.vehStats.normalDecel, vehicleComp.currentDecel, 0.1f);
            Assert.AreEqual(vehicleComp.currentMaxSpeed, vehicleComp.vehRigidBody.velocity.magnitude, 0.1f);
            var expected = TestUtils.GetExpectedHealthLoss(vehicleComp.vehStats.boostHealthDecreasePct, 
                                                        vehicleComp.maxHealth, 
                                                        vehicleComp.minBoostHealth, 
                                                        vehicleComp.boostTick, 
                                                        Time.fixedDeltaTime);
            Assert.AreEqual(expected, vehicleComp.currentHealth, 0.1f);
            Assert.AreEqual(vehicleComp.minBoostHealth, vehicleComp.currentHealth, 0.1f);
        }

        [UnityTest]
        public IEnumerator TestBoostSlowerDecel() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Accelerate
            inputMock.GetKey(KeyCode.Space).Returns(true);
            inputMock.GetKey(KeyCode.Q).Returns(true);
            yield return new WaitForSeconds(1);
            inputMock.GetKey(KeyCode.Space).Returns(false);
            inputMock.GetKey(KeyCode.Q).Returns(false);
            int timeToStopNormal = 0;
            bool condition_cb_one() { 
                timeToStopNormal++;
                return vehicleComp.vehRigidBody.velocity.magnitude > 0f;
            }
            // Await to stop normally
            yield return TestUtils.WaitWhile(condition_cb_one);
            inputMock.GetKey(KeyCode.Space).Returns(true);
            inputMock.GetKey(KeyCode.Q).Returns(true);
            yield return new WaitForSeconds(1);
            inputMock.GetKey(KeyCode.Space).Returns(false);
            inputMock.GetKey(KeyCode.Q).Returns(true);
            int timeToStopBoost = 0;
            bool condition_cb_two() { 
                timeToStopBoost++;
                return vehicleComp.vehRigidBody.velocity.magnitude > 0f;
            }
            // Await to stop while holding boost down
            yield return TestUtils.WaitWhile(condition_cb_two);
            Assert.Greater(timeToStopBoost, timeToStopNormal);
        }

        [UnityTest]
        public IEnumerator TestStillRotate() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Change delta time variable so we don't get ridiculous rotation amounts
            inputMock.GetFixedDeltaTime().Returns(0.1f);
            // Turn in place
            inputMock.GetAxisRaw("Horizontal").Returns(1.0f);
            yield return new WaitForSeconds(0.1f);
            // Check that x, z coordinates hasn't changed, but y axis rotation has in a positive direction
            Assert.AreEqual(0, vehicleComp.transform.position.x, 0.1f);
            Assert.AreEqual(0, vehicleComp.transform.position.z, 0.1f);
            Assert.Positive(vehicleComp.transform.rotation.y);
            // Reset and check the negative direction
            TestUtils.reset_position(ref vehicleComp);
            inputMock.GetAxisRaw("Horizontal").Returns(-1.0f);
            yield return new WaitForSeconds(0.1f);
            Assert.AreEqual(0, vehicleComp.transform.position.x, 0.1f);
            Assert.AreEqual(0, vehicleComp.transform.position.z, 0.1f);
            Assert.Negative(vehicleComp.transform.rotation.y);
        }

        [UnityTest]
        public IEnumerator TestTurn() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Accelerate
            inputMock.GetKey(KeyCode.Space).Returns(true);
            inputMock.GetAxisRaw("Horizontal").Returns(0.001f);
            yield return new WaitForSeconds(1);
            // Check that z and x coordinates have changed in a positive direction
            Assert.Positive(vehicleComp.transform.position.z);
            Assert.Positive(vehicleComp.transform.position.x);
            TestUtils.reset_position(ref vehicleComp);
            inputMock.GetAxisRaw("Horizontal").Returns(-0.001f);
            yield return new WaitForSeconds(1);
            Assert.Positive(vehicleComp.transform.position.z);
            Assert.Negative(vehicleComp.transform.position.x);
        }

        [UnityTest]
        public IEnumerator TestCameraFollow() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            GameObject camObj = GameObject.Find("VehicleFollowCam");
            // This is needed to allow the camera to settle before grabbing its position
            yield return new WaitForSeconds(1);
            float startingVehicleX = vehicleComp.transform.position.x;
            float startingVehicleZ = vehicleComp.transform.position.z;
            float startingCameraX = camObj.transform.position.x;
            float startingCameraZ = camObj.transform.position.z;
            // Accelerate 
            inputMock.GetKey(KeyCode.Space).Returns(true);
            yield return new WaitForSeconds(2);
            // Await stop
            inputMock.GetKey(KeyCode.Space).Returns(false);
            bool condition_cb() { return vehicleComp.vehRigidBody.velocity.magnitude > 0f;}
            yield return TestUtils.WaitWhile(condition_cb);
            // Check that camera coordinates changed proportionally
            Assert.AreEqual((vehicleComp.transform.position.x - startingVehicleX), (camObj.transform.position.x - startingCameraX), 0.5f);
            Assert.AreEqual((vehicleComp.transform.position.z - startingVehicleZ), (camObj.transform.position.z - startingCameraZ), 0.5f);
        }

        [UnityTest]
        public IEnumerator TestHeightAdjustStartHighLow() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            yield return new WaitForSeconds(2);
            // Check that our current height above the track matches the desired height
            Assert.True(Utils.Vector3Compare(new Vector3(0, vehicleComp.vehStats.desiredHeight, 0), 
                            new Vector3(vehicleComp.transform.position.x, vehicleComp.currentHeightOverTrack, vehicleComp.transform.position.z), 0.01f));
            vehicleComp.transform.position = new Vector3(0, 10, 0);
            yield return new WaitForSeconds(2);
            // Check that our current height above the track matches the desired height
            Assert.True(Utils.Vector3Compare(new Vector3(0, vehicleComp.vehStats.desiredHeight, 0), 
                            new Vector3(vehicleComp.transform.position.x, vehicleComp.currentHeightOverTrack, vehicleComp.transform.position.z), 0.01f));
            
        }

        [UnityTest]
        public IEnumerator TestFallRotateToFlat() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Need to go high enough to avoid collision whilst falling
            vehicleComp.transform.position = new Vector3(0, 200, 0);
            vehicleComp.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 90));
            yield return new WaitForSeconds(4);
            // Check that our current height above the track matches the desired height
            Assert.True(Utils.Vector3Compare(new Vector3(0, vehicleComp.vehStats.desiredHeight, 0), 
                            new Vector3(vehicleComp.transform.position.x, vehicleComp.currentHeightOverTrack, vehicleComp.transform.position.z), 0.01f));
            Assert.True(Utils.QuaternionCompare(Quaternion.Euler(new Vector3(0, 0, 0)), vehicleComp.transform.rotation, 0.01f));
        }

        [UnityTest]
        public IEnumerator TestDeathBarrier() {
            (Vehicle vehicleComp, PlayerInputIntfc inputMock) = TestUtils.init();
            // Speed up the test
            vehicleComp.vehStats.normalMaxSpeed = 2600f;
            inputMock.GetKey(KeyCode.Space).Returns(true);
            inputMock.GetKey(KeyCode.Q).Returns(true);
            bool condition_cb() { 
                return !SceneManager.GetSceneByName("RetryCrash").isLoaded;
            }
            yield return TestUtils.WaitWhile(condition_cb, 50);
            Assert.True(SceneManager.GetSceneByName("RetryCrash").isLoaded);
        }
    }
}
