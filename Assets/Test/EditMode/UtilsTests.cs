using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class UtilsTests {
    [Test]
    public void TestQuaternionCompareTrue() {
        Quaternion q1 = new Quaternion(0, 0, 0, 0);
        Quaternion q2 = new Quaternion(0.09f, 0.05f, 0.1f, 0f);
        float allowedDiff = 0.1f;
        Assert.True(Utils.QuaternionCompare(q1, q2, allowedDiff));
    }

    [Test]
    public void TestQuaternionCompareFalseX() {
        Quaternion q1 = new Quaternion(0, 0, 0, 0);
        Quaternion q2 = new Quaternion(0.1f, 0.09f, 0f, 0f);
        float allowedDiff = 0.09f;
        Assert.False(Utils.QuaternionCompare(q1, q2, allowedDiff));
    }

    [Test]
    public void TestQuaternionCompareFalseY() {
        Quaternion q1 = new Quaternion(0, 0, 0, 0);
        Quaternion q2 = new Quaternion(0.09f, 0.1f, 0f, 0f);
        float allowedDiff = 0.09f;
        Assert.False(Utils.QuaternionCompare(q1, q2, allowedDiff));
    }

    [Test]
    public void TestQuaternionCompareFalseZ() {
        Quaternion q1 = new Quaternion(0, 0, 0, 0);
        Quaternion q2 = new Quaternion(0.09f, 0.05f, 0.1f, 0f);
        float allowedDiff = 0.09f;
        Assert.False(Utils.QuaternionCompare(q1, q2, allowedDiff));
    }

    [Test]
    public void TestQuaternionCompareFalseW() {
        Quaternion q1 = new Quaternion(0, 0, 0, 0);
        Quaternion q2 = new Quaternion(0.09f, 0.05f, 0, 0.1f);
        float allowedDiff = 0.09f;
        Assert.False(Utils.QuaternionCompare(q1, q2, allowedDiff));
    }

    [Test]
    public void TestVector3CompareTrue() {
        Vector3 v1 = new Vector3(0, 0, 0);
        Vector3 v2 = new Vector3(0.09f, 0.05f, 0.1f);
        float allowedDiff = 0.1f;
        Assert.True(Utils.Vector3Compare(v1, v2, allowedDiff));
    }

    [Test]
    public void TestVector3CompareFalseX() {
        Vector3 v1 = new Vector3(0, 0, 0);
        Vector3 v2 = new Vector3(0.1f, 0.0f, 0.0f);
        float allowedDiff = 0.09f;
        Assert.False(Utils.Vector3Compare(v1, v2, allowedDiff));
    }

    [Test]
    public void TestVector3CompareFalseY() {
        Vector3 v1 = new Vector3(0, 0, 0);
        Vector3 v2 = new Vector3(0.09f, 0.1f, 0.0f);
        float allowedDiff = 0.09f;
        Assert.False(Utils.Vector3Compare(v1, v2, allowedDiff));
    }

    [Test]
    public void TestVector3CompareFalseZ() {
        Vector3 v1 = new Vector3(0, 0, 0);
        Vector3 v2 = new Vector3(0.09f, 0.05f, 0.1f);
        float allowedDiff = 0.09f;
        Assert.False(Utils.Vector3Compare(v1, v2, allowedDiff));
    }

    [Test]
    public void TestVector3LimitLerpLerp() {
        Vector3 v1 = new Vector3(0, 0, 0);
        Vector3 v2 = new Vector3(1f, 3.5f, 0.1f);
        float allowedDiff = 0.1f;
        Assert.True(Utils.Vector3Compare(new Vector3(0.4f, 1.4f, 0.04f), Utils.Vector3LimitLerp(v1, v2, allowedDiff, 0.4f), 0.01f));
    }

    [Test]
    public void TestVector3LimitLerpLimit() {
        Vector3 v1 = new Vector3(0, 0, 0);
        Vector3 v2 = new Vector3(0.5f, 0.3f, 0.1f);
        float allowedDiff = 0.5f;
        Assert.True(Utils.Vector3Compare(v2, Utils.Vector3LimitLerp(v1, v2, allowedDiff, 0.4f), 0.01f));
    }

    [Test]
    public void TestTimeFloatToString() {
        float currentTime = 100.550f;
        Assert.AreEqual(Utils.TimeFloatToString(currentTime), "01:40:550");
    }

    [Test]
    public void TestUpdateBestTimeBetter() {
        PersistData oldData = new PersistData();
        oldData.trackTimeDict["TestTrack1"] = new TrackTime("02:50:600");
        DataMgr.SaveData(oldData);

        float timeSeconds = 100.5f;
        PersistData newData = new PersistData();
        newData.trackTimeDict["TestTrack1"] = new TrackTime(Utils.TimeFloatToString(timeSeconds));

        Assert.True(Utils.UpdateBestTime("TestTrack1", timeSeconds));
        Assert.AreEqual(newData.trackTimeDict["TestTrack1"], DataMgr.LoadData().trackTimeDict["TestTrack1"]);
    }

    [Test]
    public void TestUpdateBestTimeWorse() {
        PersistData oldData = new PersistData();
        TrackTime oldTime = new TrackTime("02:50:600");
        oldData.trackTimeDict["TestTrack1"] = oldTime;
        DataMgr.SaveData(oldData);

        float timeSeconds = 300.5f;

        Assert.False(Utils.UpdateBestTime("TestTrack1", timeSeconds));
        Assert.AreEqual(oldData.trackTimeDict["TestTrack1"], oldTime);
    }

    [Test]
    public void TestUpdateBestTimeNonExistentTrack() {
        PersistData oldData = new PersistData();
        oldData.trackTimeDict["TestTrack1"] = new TrackTime("02:50:600");
        oldData.trackTimeDict["TestTrack2"] = new TrackTime("02:50:600");
        DataMgr.SaveData(oldData);

        float timeSeconds = 300.5f;

        Assert.True(Utils.UpdateBestTime("TestTrack3", timeSeconds));
        Assert.AreEqual(oldData.trackTimeDict["TestTrack1"], DataMgr.LoadData().trackTimeDict["TestTrack1"]);
        Assert.AreEqual(oldData.trackTimeDict["TestTrack2"], DataMgr.LoadData().trackTimeDict["TestTrack2"]);
        Assert.AreEqual(new TrackTime(Utils.TimeFloatToString(timeSeconds)), DataMgr.LoadData().trackTimeDict["TestTrack3"]);
    }

    [Test]
    public void TestLoadBestTimeExists() {
        PersistData oldData = new PersistData();
        string timeToCheck = "02:50:600";
        oldData.trackTimeDict["TestTrack1"] = new TrackTime(timeToCheck);
        DataMgr.SaveData(oldData);

        Assert.AreEqual(timeToCheck, Utils.LoadBestTime("TestTrack1"));
    }

    [Test]
    public void TestLoadBestTimeDoesntExist() {
        PersistData oldData = new PersistData();
        string defaultTime = "00:00:000";
        Assert.AreEqual(defaultTime, Utils.LoadBestTime("NonExistentTrack"));
    }
}
