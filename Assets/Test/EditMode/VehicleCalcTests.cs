using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class VehicleCalcTests {
    [Test]
    public void TestCalcVelocitySlice() {
        float acceleration = 10f;
        float timeDelta = 0.2f;
        Assert.AreEqual(acceleration * timeDelta, VehicleCalcs.CalcVelocityDelta(acceleration, timeDelta));
    }

    [Test]
    public void TestIncreaseVelocity() {
        float currentSpeed = 0f;
        float startingCurrentSpeed = currentSpeed;
        float velocityDelta = 10f;
        float maxSpeed = 100f;
        Assert.AreEqual(startingCurrentSpeed + velocityDelta, VehicleCalcs.CalcSpeedThrustOn(velocityDelta, maxSpeed, currentSpeed), 0.1f);
    }

    [Test]
    public void TestDecreaseVelocity() {
        float currentSpeed = 20f;
        float startingCurrentSpeed = currentSpeed;
        float velocityDelta = 10f;
        Assert.AreEqual(startingCurrentSpeed - velocityDelta, VehicleCalcs.CalcSpeedThrustOff(velocityDelta, currentSpeed), 0.1f);
    }

    [Test]
    public void TestDontExceedMaxSpeed() {
        float maxSpeed = 100f;
        float velocityDelta = 10f;
        float currentSpeed = maxSpeed;
        Assert.AreEqual(maxSpeed, VehicleCalcs.CalcSpeedThrustOn(velocityDelta, maxSpeed, currentSpeed), 0.1f);
    }

    [Test]
    public void TestDecelFromHigherThanMaxSpeed() {
        float maxSpeed = 100f;
        float velocityDelta = 10f;
        float currentSpeed = maxSpeed + 2*velocityDelta;
        Assert.AreEqual(currentSpeed - velocityDelta, VehicleCalcs.CalcSpeedThrustOn(velocityDelta, maxSpeed, currentSpeed), 0.1f);
    }

    [Test]
    public void TestNoNegativeVelocity() {
        float currentSpeed = 5f;
        float velocityDelta = 10f;
        Assert.AreEqual(0f, VehicleCalcs.CalcSpeedThrustOff(velocityDelta, currentSpeed));
    }

    [Test]
    public void TestCalcYawNoDrift() {
        float turnAmount = 1;
        float turnSpeed = 100;
        bool driftInpActive = false;
        float driftMulti = 1.5f;
        float fixedDeltaTime = 1;
        Assert.True(Utils.QuaternionCompare(
                    VehicleCalcs.CalcTurn(
                        turnAmount, turnSpeed, driftInpActive, driftMulti, fixedDeltaTime), 
                        new Quaternion(0.00000f, 0.76604f, 0.00000f, 0.64279f), 0.1f));
    }

    [Test]
    public void TestCalcYawDrift() {
        float turnAmount = 1;
        float turnSpeed = 100;
        bool driftInpActive = true;
        float driftMulti = 1.5f;
        float fixedDeltaTime = 1;
        Assert.True(Utils.QuaternionCompare(
                    VehicleCalcs.CalcTurn(
                        turnAmount, turnSpeed, driftInpActive, driftMulti, fixedDeltaTime), 
                        new Quaternion(-0.93301f, 0.25000f, -0.25000f, 0.06699f), 0.1f));
    }

    [Test]
    public void TestCalcHeightAdjustHigh() {
        float heightOverTrack = 20;
        float desiredHeight = 15;
        float heightAdjustRate = 100;
        float fixedDeltaTime = 0.5f;
        Assert.AreEqual(VehicleCalcs.CalcHeightAdjust(heightOverTrack, desiredHeight, heightAdjustRate, fixedDeltaTime), -5f, 0.1f);
    }

    [Test]
    public void TestCalcHeightAdjustLow() {
        float heightOverTrack = 10;
        float desiredHeight = 15;
        float heightAdjustRate = 100;
        float fixedDeltaTime = 0.5f;
        Assert.AreEqual(VehicleCalcs.CalcHeightAdjust(heightOverTrack, desiredHeight, heightAdjustRate, fixedDeltaTime), 5f, 0.1f);
    }

    [Test]
    public void TestCalcHealthDelta() {
        float pctOfMaxHealth = 0.01f;
        int maxHealth = 100;
        float fixedDeltaTime = 1f;
        Assert.AreEqual(pctOfMaxHealth * maxHealth * fixedDeltaTime, VehicleCalcs.CalcHealthDelta(pctOfMaxHealth, maxHealth, fixedDeltaTime), 0.1f);
    }

    [Test]
    public void TestCalcBoostActive() {
        bool boostOn = true;
        float currentHealth = 100f;
        float minBoostHealth = 1f;
        var boostValid = VehicleCalcs.CalcBoostValid(boostOn, currentHealth, minBoostHealth);
        Assert.True(boostValid);
    }

    [Test]
    public void TestCalcBoostInactive() {
        bool boostOn = false;
        float currentHealth = 100f;
        float minBoostHealth = 1f;
        var boostValid = VehicleCalcs.CalcBoostValid(boostOn, currentHealth, minBoostHealth);
        Assert.False(boostValid);
    }

    [Test]
    public void TestCalcBoostWithMinHealth() {
        bool boostOn = true;
        float currentHealth = 0.1f;
        float minBoostHealth = currentHealth;
        var boostValid = VehicleCalcs.CalcBoostValid(boostOn, currentHealth, minBoostHealth);
        Assert.False(boostValid);
    }

    [Test]
    public void TestCalcHealthLoss() {
        float currentHealth = 100; 
        float pctOfMaxHealth = 0.002f; 
        float maxHealth = 100; 
        float minHealth = 0;
        float fixedDeltaTime = 1f;
        float healthDelta = VehicleCalcs.CalcHealthDelta(pctOfMaxHealth, maxHealth, fixedDeltaTime);
        Assert.AreEqual(currentHealth - healthDelta, VehicleCalcs.CalcHealthLoss(currentHealth, healthDelta, minHealth), 0.1f);
    }

    [Test]
    public void TestCalcHealthLossMin() {
        float currentHealth = 1; 
        float pctOfMaxHealth = 0.02f; 
        float maxHealth = 100; 
        float minHealth = 0;
        float fixedDeltaTime = 1f;
        float healthDelta = VehicleCalcs.CalcHealthDelta(pctOfMaxHealth, maxHealth, fixedDeltaTime);
        Assert.AreEqual(minHealth, VehicleCalcs.CalcHealthLoss(currentHealth, healthDelta, minHealth), 0.1f);
    }

    [Test]
    public void TestCalcHealthGain() {
        float currentHealth = 90; 
        float pctOfMaxHealth = 0.002f; 
        float maxHealth = 100; 
        float fixedDeltaTime = 1f;
        float healthDelta = VehicleCalcs.CalcHealthDelta(pctOfMaxHealth, maxHealth, fixedDeltaTime);
        Assert.AreEqual(currentHealth + healthDelta, VehicleCalcs.CalcHealthGain(currentHealth, healthDelta, maxHealth), 0.1f);
    }

    [Test]
    public void TestCalcHealthGainMax() {
        float currentHealth = 99; 
        float pctOfMaxHealth = 0.02f; 
        float maxHealth = 100; 
        float fixedDeltaTime = 1f;
        float healthDelta = VehicleCalcs.CalcHealthDelta(pctOfMaxHealth, maxHealth, fixedDeltaTime);
        Assert.AreEqual(maxHealth, VehicleCalcs.CalcHealthGain(currentHealth, healthDelta, maxHealth), 0.1f);
    }
}
