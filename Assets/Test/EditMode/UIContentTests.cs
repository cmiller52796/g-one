using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class UIContentTests {
    [Test]
    public void TestSpeedString() {
        Assert.AreEqual(MakeUIContent.MakeSpeedometerString(10f), "10 m/s");
    }

    [Test]
    public void TestUpdateHealth() {
        float currentHealth = 10f;
        float maxHealth = 100f;
        Assert.AreEqual(MakeUIContent.UpdateHealth(currentHealth, maxHealth), currentHealth/maxHealth, 0.1f);
    }

    [Test]
    public void TestMakeLapString() {
        int currentLap = 1;
        int maxLaps = 3;
        Assert.AreEqual(MakeUIContent.MakeLapString(currentLap, maxLaps), "Lap: 1/3");
    }

    [Test]
    public void TestMakeCurrentTime() {
        float currentTime = 100.550f;
        Assert.AreEqual(MakeUIContent.MakeCurrentTime(currentTime), "Time: 01:40:550");
    }
}
