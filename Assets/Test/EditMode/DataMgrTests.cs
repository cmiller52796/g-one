using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System.IO;

public class DataMgrTests {
    [Test]
    // Can use the class itself to test both ways
    public void TestSaveAndLoadData() {
        PersistData pdToSave = new PersistData();
        pdToSave.trackTimeDict["TestTrack1"] = new TrackTime("55:55:555");
        DataMgr.SaveData(pdToSave);
        PersistData pdToLoad = DataMgr.LoadData();
        Assert.AreEqual(pdToSave.trackTimeDict["TestTrack1"], pdToLoad.trackTimeDict["TestTrack1"]);
    }

    [Test]
    public void TestSaveDataNoSaveDir() {
        PersistData pdToSave = new PersistData();
        pdToSave.trackTimeDict["TestTrack1"] = new TrackTime("55:55:555");
        File.Delete(DataMgr.fullSavePath);
        Directory.Delete(DataMgr.fullSaveDir);
        DataMgr.SaveData(pdToSave);
        PersistData pdToLoad = DataMgr.LoadData();
        Assert.AreEqual(pdToSave.trackTimeDict["TestTrack1"], pdToLoad.trackTimeDict["TestTrack1"]);
    }

    [Test]
    public void TestLoadDataNoSaveFile() {
        PersistData pdToSave = new PersistData();
        if (File.Exists(DataMgr.fullSavePath)) {
            File.Delete(DataMgr.fullSavePath);
        }
        PersistData pdToLoad = DataMgr.LoadData();
        Assert.AreEqual(pdToSave.trackTimeDict.Count == 0, pdToLoad.trackTimeDict.Count == 0);
    }
}
