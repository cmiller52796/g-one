using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System.IO;

public class PersistDataTests {
    [Test]
    public void TestEquals() {
        PersistData pdOne = new PersistData();
        pdOne.trackTimeDict["TestTrack1"] = new TrackTime("55:55:555");
        PersistData pdTwo = new PersistData();
        pdTwo.trackTimeDict["TestTrack1"] = new TrackTime("55:55:555");
        Assert.True(pdOne.trackTimeDict["TestTrack1"] == pdTwo.trackTimeDict["TestTrack1"]);
    }

    [Test]
    public void TestLessThanMinutesTrue() {
        PersistData pdOne = new PersistData();
        pdOne.trackTimeDict["TestTrack1"] = new TrackTime("3:30:500");
        PersistData pdTwo = new PersistData();
        pdTwo.trackTimeDict["TestTrack1"] = new TrackTime("5:55:555");
        Assert.True(pdOne.trackTimeDict["TestTrack1"] < pdTwo.trackTimeDict["TestTrack1"]);
    }

    [Test]
    public void TestLessThanSecondsTrue() {
        PersistData pdOne = new PersistData();
        pdOne.trackTimeDict["TestTrack1"] = new TrackTime("5:30:500");
        PersistData pdTwo = new PersistData();
        pdTwo.trackTimeDict["TestTrack1"] = new TrackTime("5:55:555");
        Assert.True(pdOne.trackTimeDict["TestTrack1"] < pdTwo.trackTimeDict["TestTrack1"]);
    }

    [Test]
    public void TestLessThanMillisecondsTrue() {
        PersistData pdOne = new PersistData();
        pdOne.trackTimeDict["TestTrack1"] = new TrackTime("5:55:500");
        PersistData pdTwo = new PersistData();
        pdTwo.trackTimeDict["TestTrack1"] = new TrackTime("5:55:555");
        Assert.True(pdOne.trackTimeDict["TestTrack1"] < pdTwo.trackTimeDict["TestTrack1"]);
    }

    [Test]
    public void TestLessThanMinutesFalse() {
        PersistData pdOne = new PersistData();
        pdOne.trackTimeDict["TestTrack1"] = new TrackTime("5:55:555");
        PersistData pdTwo = new PersistData();
        pdTwo.trackTimeDict["TestTrack1"] = new TrackTime("3:30:500");
        Assert.False(pdOne.trackTimeDict["TestTrack1"] < pdTwo.trackTimeDict["TestTrack1"]);
    }

    [Test]
    public void TestLessThanSecondsFalse() {
        PersistData pdOne = new PersistData();
        pdOne.trackTimeDict["TestTrack1"] = new TrackTime("5:55:555");
        PersistData pdTwo = new PersistData();
        pdTwo.trackTimeDict["TestTrack1"] = new TrackTime("5:30:500");
        Assert.False(pdOne.trackTimeDict["TestTrack1"] < pdTwo.trackTimeDict["TestTrack1"]);
    }

    [Test]
    public void TestLessThanMillisecondsFalse() {
        PersistData pdOne = new PersistData();
        pdOne.trackTimeDict["TestTrack1"] = new TrackTime("5:55:555");
        PersistData pdTwo = new PersistData();
        pdTwo.trackTimeDict["TestTrack1"] = new TrackTime("5:55:500");
        Assert.False(pdOne.trackTimeDict["TestTrack1"] < pdTwo.trackTimeDict["TestTrack1"]);
    }

    [Test]
    public void TestGreaterThanMinutesTrue() {
        PersistData pdOne = new PersistData();
        pdOne.trackTimeDict["TestTrack1"] = new TrackTime("5:55:555");
        PersistData pdTwo = new PersistData();
        pdTwo.trackTimeDict["TestTrack1"] = new TrackTime("3:30:500");
        Assert.True(pdOne.trackTimeDict["TestTrack1"] > pdTwo.trackTimeDict["TestTrack1"]);
    }

    [Test]
    public void TestGreaterThanSecondsTrue() {
        PersistData pdOne = new PersistData();
        pdOne.trackTimeDict["TestTrack1"] = new TrackTime("5:55:555");
        PersistData pdTwo = new PersistData();
        pdTwo.trackTimeDict["TestTrack1"] = new TrackTime("5:30:500");
        Assert.True(pdOne.trackTimeDict["TestTrack1"] > pdTwo.trackTimeDict["TestTrack1"]);
    }

    [Test]
    public void TestGreaterThanMillisecondsTrue() {
        PersistData pdOne = new PersistData();
        pdOne.trackTimeDict["TestTrack1"] = new TrackTime("5:55:555");
        PersistData pdTwo = new PersistData();
        pdTwo.trackTimeDict["TestTrack1"] = new TrackTime("5:55:500");
        Assert.True(pdOne.trackTimeDict["TestTrack1"] > pdTwo.trackTimeDict["TestTrack1"]);
    }

    [Test]
    public void TestGreaterThanMinutesFalse() {
        PersistData pdOne = new PersistData();
        pdOne.trackTimeDict["TestTrack1"] = new TrackTime("3:30:500");
        PersistData pdTwo = new PersistData();
        pdTwo.trackTimeDict["TestTrack1"] = new TrackTime("5:55:555");
        Assert.False(pdOne.trackTimeDict["TestTrack1"] > pdTwo.trackTimeDict["TestTrack1"]);
    }

    [Test]
    public void TestGreaterThanSecondsFalse() {
        PersistData pdOne = new PersistData();
        pdOne.trackTimeDict["TestTrack1"] = new TrackTime("5:30:500");
        PersistData pdTwo = new PersistData();
        pdTwo.trackTimeDict["TestTrack1"] = new TrackTime("5:55:555");
        Assert.False(pdOne.trackTimeDict["TestTrack1"] > pdTwo.trackTimeDict["TestTrack1"]);
    }

    [Test]
    public void TestGreaterThanMillisecondsFalse() {
        PersistData pdOne = new PersistData();
        pdOne.trackTimeDict["TestTrack1"] = new TrackTime("5:55:500");
        PersistData pdTwo = new PersistData();
        pdTwo.trackTimeDict["TestTrack1"] = new TrackTime("5:55:555");
        Assert.False(pdOne.trackTimeDict["TestTrack1"] > pdTwo.trackTimeDict["TestTrack1"]);
    }
}
